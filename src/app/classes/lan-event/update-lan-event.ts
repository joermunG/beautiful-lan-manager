import {JsonObject, JsonProperty} from 'json2typescript';
import {DateConverter} from '../converters/date-converter';

@JsonObject('UpdateLanEvent')
export class UpdateLanEvent {
  @JsonProperty('name', String)
  private _name: string = null;
  @JsonProperty('startDate', DateConverter)
  private _startDate: Date = null;
  @JsonProperty('endDate', DateConverter)
  private _endDate: Date = null;

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get startDate(): Date {
    return this._startDate;
  }

  set startDate(value: Date) {
    this._startDate = value;
  }

  get endDate(): Date {
    return this._endDate;
  }

  set endDate(value: Date) {
    this._endDate = value;
  }
}
