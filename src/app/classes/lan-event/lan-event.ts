import {JsonObject, JsonProperty} from 'json2typescript';
import {DateConverter} from '../converters/date-converter';

@JsonObject('LanEvent')
export class LanEvent {
  @JsonProperty('id', Number)
  private _id: number = null;
  @JsonProperty('name', String)
  private _name: string = null;
  @JsonProperty('startDate', DateConverter)
  private _startDate: Date = null;
  @JsonProperty('endDate', DateConverter)
  private _endDate: Date = null;
  @JsonProperty('participantsCount', Number)
  private _participantsCount: number = null;
  @JsonProperty('tournamentsCount', Number)
  private _tournamentsCount: number = null;

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get startDate(): Date {
    return this._startDate;
  }

  set startDate(value: Date) {
    this._startDate = value;
  }

  get endDate(): Date {
    return this._endDate;
  }

  set endDate(value: Date) {
    this._endDate = value;
  }

  get participantsCount(): number {
    return this._participantsCount;
  }

  set participantsCount(value: number) {
    this._participantsCount = value;
  }

  get tournamentsCount(): number {
    return this._tournamentsCount;
  }

  set tournamentsCount(value: number) {
    this._tournamentsCount = value;
  }
}
