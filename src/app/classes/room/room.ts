import {LanEvent} from '../lan-event/lan-event';
import {GridSquare} from './grid-square';
import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('Room')
export class Room {
  @JsonProperty('id', Number)
  private _id: number = null;
  @JsonProperty('name', String)
  private _name: string = null;
  @JsonProperty('sizeX', Number)
  private _sizeX: number = null;
  @JsonProperty('sizeY', Number)
  private _sizeY: number = null;
  @JsonProperty('countSeats', Number)
  private _countSeats: number = null;
  @JsonProperty('eventId', Number)
  private _eventId: number = null;
  @JsonProperty('event', LanEvent)
  private _event: LanEvent = null;
  @JsonProperty('gridSquares', [GridSquare])
  private _gridSquares: GridSquare[] = [];

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get sizeX(): number {
    return this._sizeX;
  }

  set sizeX(value: number) {
    this._sizeX = value;
  }

  get sizeY(): number {
    return this._sizeY;
  }

  set sizeY(value: number) {
    this._sizeY = value;
  }

  get countSeats(): number {
    return this._countSeats;
  }

  set countSeats(value: number) {
    this._countSeats = value;
  }

  get eventId(): number {
    return this._eventId;
  }

  set eventId(value: number) {
    this._eventId = value;
  }

  get event(): LanEvent {
    return this._event;
  }

  set event(value: LanEvent) {
    this._event = value;
  }

  get gridSquares(): GridSquare[] {
    return this._gridSquares;
  }

  set gridSquares(value: GridSquare[]) {
    this._gridSquares = value;
  }
}

