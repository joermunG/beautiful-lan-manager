import {JsonObject, JsonProperty} from 'json2typescript';
import {GridSquareRotation} from '../../enums/grid-square-rotation';
import {GridSquareType} from '../../enums/grid-square-type';
import {User} from '../user/user';

@JsonObject('GridSquare')
export class GridSquare {
  @JsonProperty('id', Number)
  private _id: number = null;
  @JsonProperty('x', Number)
  private _x: number = null;
  @JsonProperty('y', Number)
  private _y: number = null;
  @JsonProperty('type', String)
  private _type: GridSquareType = null;
  @JsonProperty('roomId', Number)
  private _roomId: number = null;
  @JsonProperty('rotation', String)
  private _rotation: GridSquareRotation = null;
  @JsonProperty('user', User)
  private _user: User = null;

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get x(): number {
    return this._x;
  }

  set x(value: number) {
    this._x = value;
  }

  get y(): number {
    return this._y;
  }

  set y(value: number) {
    this._y = value;
  }

  get type(): GridSquareType {
    return this._type;
  }

  set type(value: GridSquareType) {
    this._type = value;
  }

  get rotation(): GridSquareRotation {
    return this._rotation;
  }

  set rotation(value: GridSquareRotation) {
    this._rotation = value;
  }

  get roomId(): number {
    return this._roomId;
  }

  set roomId(value: number) {
    this._roomId = value;
  }

  get user(): User {
    return this._user;
  }

  set user(value: User) {
    this._user = value;
  }
}
