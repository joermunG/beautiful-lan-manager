import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('GridCoordinate')
export class GridCoordinate {
  @JsonProperty('x', Number)
  private _x: number = null;
  @JsonProperty('y', Number)
  private _y: number = null;

  get x(): number {
    return this._x;
  }

  set x(value: number) {
    this._x = value;
  }

  get y(): number {
    return this._y;
  }

  set y(value: number) {
    this._y = value;
  }
}
