import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('CreateRoom')
export class CreateRoom {
  @JsonProperty('name', String)
  private _name: string = null;
  @JsonProperty('sizeX', Number)
  private _sizeX: number = null;
  @JsonProperty('sizeY', Number)
  private _sizeY: number = null;
  @JsonProperty('eventId', Number)
  private _eventId: number = null;

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get sizeX(): number {
    return this._sizeX;
  }

  set sizeX(value: number) {
    this._sizeX = value;
  }

  get sizeY(): number {
    return this._sizeY;
  }

  set sizeY(value: number) {
    this._sizeY = value;
  }

  get eventId(): number {
    return this._eventId;
  }

  set eventId(value: number) {
    this._eventId = value;
  }
}
