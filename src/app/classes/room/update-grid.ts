import {GridCoordinate} from './grid-coordinate';
import {GridSquareType} from '../../enums/grid-square-type';
import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('UpdateGrid')
export class UpdateGrid {
  @JsonProperty('grid', GridCoordinate)
  private _grid: GridCoordinate = null;
  @JsonProperty('type', String)
  private _type: GridSquareType = null;

  get grid(): GridCoordinate {
    return this._grid;
  }

  set grid(value: GridCoordinate) {
    this._grid = value;
  }

  get type(): GridSquareType {
    return this._type;
  }

  set type(value: GridSquareType) {
    this._type = value;
  }
}
