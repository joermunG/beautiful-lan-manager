import {JsonObject, JsonProperty} from 'json2typescript';
import {GridCoordinate} from './grid-coordinate';

@JsonObject('PlaceUser')
export class PlaceUser {
  @JsonProperty('grid', GridCoordinate)
  private _grid: GridCoordinate = null;
  @JsonProperty('userId')
  private _userId: number = null;

  get grid(): GridCoordinate {
    return this._grid;
  }

  set grid(value: GridCoordinate) {
    this._grid = value;
  }

  get userId(): number {
    return this._userId;
  }

  set userId(value: number) {
    this._userId = value;
  }
}
