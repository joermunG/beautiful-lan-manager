import {JsonObject, JsonProperty} from 'json2typescript';
import {isNull} from 'util';
import {TournamentScore} from './tournament-score';

@JsonObject('TournamentMatch')
export class TournamentMatch {
  @JsonProperty('id', Number)
  private _id: number = null;
  @JsonProperty('matchIndex', Number)
  private _matchIndex: number = null;
  @JsonProperty('scores', [TournamentScore])
  private _scores: TournamentScore[] = [];

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get scores(): TournamentScore[] {
    return this._scores;
  }

  set scores(value: TournamentScore[]) {
    this._scores = value;
  }

  get matchIndex(): number {
    return this._matchIndex;
  }

  set matchIndex(value: number) {
    this._matchIndex = value;
  }

  get isCompleted(): boolean {
    let isCompleted = false;
    this.scores.forEach(s => {
      if (!isCompleted && !isNull(s.score) && s.score > 0) {
        isCompleted = true;
      }
    });
    return isCompleted;
  }

  get isDue(): boolean {
    let isDue = true;
    if (this.isCompleted) {
      return false;
    }
    this.scores.forEach(s => {
      isDue = isDue && (s.tournamentTeam && s.tournamentTeam.name !== 'Bye');
    });
    return isDue;
  }

  get winnerScore(): TournamentScore | null {
    const winnerScore = [...this.scores].sort((a, b) => {
      return a.score < b.score ? 1 : (a.score === b.score ? 0 : -1);
    }).shift();
    return winnerScore.score > 0 ? winnerScore : null;
  }
}

