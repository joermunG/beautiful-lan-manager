import {TournamentMode} from '../../enums/tournament-mode';
import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('CreateTournament')
export class CreateTournament {
  @JsonProperty('name', String)
  private _name: string = null;
  @JsonProperty('eventId', Number)
  private _eventId: number = null;
  @JsonProperty('mode', String)
  private _mode: TournamentMode = null;
  @JsonProperty('playerPerTeam', Number)
  private _playerPerTeam: number = null;
  @JsonProperty('image', String)
  private _image: string = null;

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get eventId(): number {
    return this._eventId;
  }

  set eventId(value: number) {
    this._eventId = value;
  }

  get mode(): TournamentMode {
    return this._mode;
  }

  set mode(value: TournamentMode) {
    this._mode = value;
  }

  get playerPerTeam(): number {
    return this._playerPerTeam;
  }

  set playerPerTeam(value: number) {
    this._playerPerTeam = value;
  }

  get image(): string {
    return this._image;
  }

  set image(value: string) {
    this._image = value;
  }
}
