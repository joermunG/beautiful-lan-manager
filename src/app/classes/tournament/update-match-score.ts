import {JsonProperty} from 'json2typescript';

export class UpdateMatchScore {
  @JsonProperty('scores')
  public _scores: UpdateMatchScoreItem[] = [];

  constructor() {
    this._scores = [];
  }

  get scores(): UpdateMatchScoreItem[] {
    return this._scores;
  }
}

interface UpdateMatchScoreItem {
  tournamentTeamId: number;
  score: number;
}
