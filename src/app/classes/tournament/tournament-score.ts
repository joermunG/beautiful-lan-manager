import {JsonObject, JsonProperty} from 'json2typescript';
import {TournamentTeam} from '../tournament-teams/tournament-team';

@JsonObject('TournamentScore')
export class TournamentScore {
  @JsonProperty('id', Number)
  private _id: number = null;
  @JsonProperty('score', Number)
  private _score: number = null;
  @JsonProperty('tournamentTeam', TournamentTeam)
  private _tournamentTeam: TournamentTeam = null;

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get score(): number {
    return this._score;
  }

  set score(value: number) {
    this._score = value;
  }

  get tournamentTeam(): TournamentTeam {
    return this._tournamentTeam;
  }

  set tournamentTeam(value: TournamentTeam) {
    this._tournamentTeam = value;
  }
}
