import {JsonObject, JsonProperty} from 'json2typescript';
import {NgttTournament} from 'ng-tournament-tree';
import {EntityPermission} from '../../enums/entity-permission';
import {TournamentMode} from '../../enums/tournament-mode';
import {TournamentState} from '../../enums/tournament-state';
import {LanEvent} from '../lan-event/lan-event';
import {TournamentTeam} from '../tournament-teams/tournament-team';
import {User} from '../user/user';
import {TournamentRound} from './tournament-round';

@JsonObject('Tournament')
export class Tournament implements NgttTournament {
  @JsonProperty('id', Number)
  private _id: number = null;
  @JsonProperty('name', String)
  private _name: string = null;
  @JsonProperty('event', LanEvent)
  private _event: LanEvent = null;
  @JsonProperty('mode', String)
  private _mode: TournamentMode = null;
  @JsonProperty('playerPerTeam', Number)
  private _playerPerTeam: number = null;
  @JsonProperty('teams', [TournamentTeam])
  private _teams: TournamentTeam[] = [];
  @JsonProperty('state', String)
  private _state: TournamentState = null;
  @JsonProperty('imageUrl', String)
  private _imageUrl: string = null;
  @JsonProperty('rounds', [TournamentRound])
  private _rounds: TournamentRound[] = [];

  /**
   *  Returns TournamentTeam of given User where they are either Member or Owner
   */
  public getUserTeam(user: User) {
    let userTeam: TournamentTeam = null;
    this.teams.forEach(team => {
      team.members.forEach(m => {
        if (m.permission !== EntityPermission.INVITEE && m.member.id === user.id) {
          userTeam = team;
        }
      });
    });
    return userTeam;
  }

  /**
   * A tournament is locked after it has been started
   * Not Planning &&
   * Not OpenForRegistration &&
   * Not Closed
   */
  public get isLocked(): boolean {
    return this.state !== TournamentState.PLANNING &&
      this.state !== TournamentState.OPEN_FOR_REGISTRATION &&
      this.state !== TournamentState.CLOSED;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get event(): LanEvent {
    return this._event;
  }

  set event(value: LanEvent) {
    this._event = value;
  }

  get mode(): TournamentMode {
    return this._mode;
  }

  set mode(value: TournamentMode) {
    this._mode = value;
  }

  get playerPerTeam(): number {
    return this._playerPerTeam;
  }

  set playerPerTeam(value: number) {
    this._playerPerTeam = value;
  }

  get teams(): TournamentTeam[] {
    return this._teams;
  }

  set teams(value: TournamentTeam[]) {
    this._teams = value;
  }

  get humanTeams(): TournamentTeam[] {
    return this._teams.filter(team => team.name !== 'Bye');
  }

  get state(): TournamentState {
    return this._state;
  }

  set state(value: TournamentState) {
    this._state = value;
  }

  get rounds(): TournamentRound[] {
    return this._rounds;
  }

  set rounds(value: TournamentRound[]) {
    this._rounds = value;
  }

  get tournament() {
    return this.name;
  }

  get imageUrl(): string {
    return this._imageUrl;
  }

  set imageUrl(value: string) {
    this._imageUrl = value;
  }
}
