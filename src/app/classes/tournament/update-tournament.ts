import {TournamentMode} from '../../enums/tournament-mode';
import {TournamentState} from '../../enums/tournament-state';
import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('UpdateTournament')
export class UpdateTournament {
  @JsonProperty('name', String)
  private _name: string = null;
  @JsonProperty('state', String)
  private _state: TournamentState = null;
  @JsonProperty('mode', String)
  private _mode: TournamentMode = null;
  @JsonProperty('playerPerTeam', Number)
  private _playerPerTeam: number = null;
  @JsonProperty('image', String)
  private _image: string = null;

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get state(): TournamentState {
    return this._state;
  }

  set state(value: TournamentState) {
    this._state = value;
  }

  get mode(): TournamentMode {
    return this._mode;
  }

  set mode(value: TournamentMode) {
    this._mode = value;
  }

  get playerPerTeam(): number {
    return this._playerPerTeam;
  }

  set playerPerTeam(value: number) {
    this._playerPerTeam = value;
  }

  get image(): string {
    return this._image;
  }

  set image(value: string) {
    this._image = value;
  }
}
