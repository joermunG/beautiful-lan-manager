import {JsonObject, JsonProperty} from 'json2typescript';
import {NgttRound} from 'ng-tournament-tree';
import {TournamentMatch} from './tournament-match';

@JsonObject('TournamentRound')
export class TournamentRound implements NgttRound {
  @JsonProperty('id', Number)
  private _id: number = null;
  @JsonProperty('type', String)
  private _type: 'Winnerbracket' | 'Loserbracket' | 'Final' = null;
  @JsonProperty('matches', [TournamentMatch])
  private _matches: TournamentMatch[] = [];

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get type(): 'Winnerbracket' | 'Loserbracket' | 'Final' {
    return this._type;
  }

  set type(value: 'Winnerbracket' | 'Loserbracket' | 'Final') {
    this._type = value;
  }

  get matches(): any[] {
    return this._matches.sort((a, b) => a.matchIndex > b.matchIndex ? 1 : -1);
  }

  set matches(value: any[]) {
    this._matches = value;
  }

  //
  // get round(): string {
  //   return this._round;
  // }
}
