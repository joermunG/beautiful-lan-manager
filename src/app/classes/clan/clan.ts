import {ClanMember} from './clan-member';
import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('Clan')
export class Clan {
  @JsonProperty('id', Number)
  private _id: number = null;
  @JsonProperty('name', String)
  private _name: string = null;
  @JsonProperty('clanTag', String)
  private _clanTag: string = null;
  @JsonProperty('members', [ClanMember])
  private _members: ClanMember[] = [];

  addMember(cm: ClanMember) {
    this.members.push(cm);
  }

  removeMember(cm: ClanMember) {
    this.members = this.members.filter(member => {
      return member.user.id !== cm.user.id;
    });
  }

  get clanOwners(): ClanMember[] {
    return this.members.filter(member => {
      return member.isOwner;
    });
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get members(): ClanMember[] {
    return this._members;
  }

  set members(value: ClanMember[]) {
    this._members = value;
  }

  get clanTag(): string {
    return this._clanTag;
  }

  set clanTag(value: string) {
    this._clanTag = value;
  }
}
