import {User} from '../user/user';
import {EntityPermission} from '../../enums/entity-permission';
import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('ClanMember')
export class ClanMember {
  @JsonProperty('user', User)
  private _user: User = null;
  @JsonProperty('userId', Number)
  private _userId: number = null;
  @JsonProperty('permission', [String])
  private _permission: EntityPermission[] = [];

  get isOwner(): boolean {
    return this.permission.includes(EntityPermission.OWNER);
  }

  get isMember(): boolean {
    return this.permission.includes(EntityPermission.MEMBER);
  }

  get isInvitee(): boolean {
    return this.permission.includes(EntityPermission.INVITEE);
  }

  public addPermission(perm: EntityPermission) {
    if (!this._permission.includes(perm)) {
      this._permission.push(perm);
    }
  }

  public removePermission(perm: string) {
    this._permission = this._permission.filter(p => {
      return p !== perm;
    });
  }

  get user(): User {
    return this._user;
  }

  set user(value: User) {
    this._user = value;
  }

  get permission(): EntityPermission[] {
    return this._permission;
  }

  get userId(): number {
    return this._userId;
  }

  set userId(value: number) {
    this._userId = value;
  }
}
