export class PagedResult<T> {
  public currentPage: number;
  public pageCount: number;
  public pageSize: number;
  public rowCount: number;
  public results: T[];
}
