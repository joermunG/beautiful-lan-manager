import {JsonProperty} from 'json2typescript';

export class UpdateUser {
  @JsonProperty('firstName', String)
  private _firstName: string = null;
  @JsonProperty('lastName', String)
  private _lastName: string = null;
  @JsonProperty('nickName', String)
  private _nickname: string = null;
  @JsonProperty('image', String)
  private _image: string = null;


  get firstName(): string {
    return this._firstName;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  get lastName(): string {
    return this._lastName;
  }

  set lastName(value: string) {
    this._lastName = value;
  }

  get nickname(): string {
    return this._nickname;
  }

  set nickname(value: string) {
    this._nickname = value;
  }

  get image(): string {
    return this._image;
  }

  set image(value: string) {
    this._image = value;
  }
}
