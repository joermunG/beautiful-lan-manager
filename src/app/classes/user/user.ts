import {JsonObject, JsonProperty} from 'json2typescript';
import {UserRole} from '../../enums/user-role';
import {LanEvent} from '../lan-event/lan-event';

@JsonObject('User')
export class User {
  @JsonProperty('id', Number)
  private _id: number = null;
  @JsonProperty('firstName', String)
  private _firstName: string = null;
  @JsonProperty('lastName', String)
  private _lastName: string = null;
  @JsonProperty('nickname', String)
  private _nickname: string = null;
  @JsonProperty('email', String)
  private _email: string = null;
  @JsonProperty('clanId', Number)
  private _clanId: number = null;
  @JsonProperty('events', [LanEvent])
  private _events: LanEvent[] = null;
  @JsonProperty('roles', [String])
  private _roles: UserRole[] = null;
  @JsonProperty('imageUrl', String)
  private _imageUrl: string | null = null;

  get isManagerOrAdmin(): boolean {
    return this._roles.includes(UserRole.MANAGER) || this._roles.includes(UserRole.ADMIN);
  }

  get isManager(): boolean {
    return this._roles.includes(UserRole.MANAGER);
  }

  get isAdmin(): boolean {
    return this._roles.includes(UserRole.MANAGER);
  }

  public hasRole(role: UserRole): boolean {
    return this._roles.includes(role);
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get firstName(): string {
    return this._firstName;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  get lastName(): string {
    return this._lastName;
  }

  set lastName(value: string) {
    this._lastName = value;
  }

  get nickname(): string {
    return this._nickname;
  }

  set nickname(value: string) {
    this._nickname = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get clanId(): number {
    return this._clanId;
  }

  set clanId(value: number) {
    this._clanId = value;
  }

  get events(): LanEvent[] {
    return this._events;
  }

  set events(value: LanEvent[]) {
    this._events = value;
  }

  get roles(): UserRole[] {
    return this._roles;
  }

  set roles(value: UserRole[]) {
    this._roles = value;
  }

  get imageUrl(): string | null {
    return this._imageUrl;
  }

  set imageUrl(value: string | null) {
    this._imageUrl = value;
  }
}
