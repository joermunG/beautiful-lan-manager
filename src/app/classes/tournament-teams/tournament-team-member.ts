import {User} from '../user/user';
import {JsonObject, JsonProperty} from 'json2typescript';
import {EntityPermission} from '../../enums/entity-permission';

@JsonObject('TournamentTeamMember')
export class TournamentTeamMember {
  @JsonProperty('id', Number)
  private _id: number = null;
  @JsonProperty('member', User)
  private _member: User = null;
  @JsonProperty('memberId', Number)
  private _memberId: number = null;
  @JsonProperty('permission', String)
  private _permission: EntityPermission = null;

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get member(): User {
    return this._member;
  }

  set member(value: User) {
    this._member = value;
  }

  get memberId(): number {
    return this._memberId;
  }

  set memberId(value: number) {
    this._memberId = value;
  }

  get permission(): EntityPermission {
    return this._permission;
  }

  set permission(value: EntityPermission) {
    this._permission = value;
  }
}
