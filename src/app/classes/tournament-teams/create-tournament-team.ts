import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('CreateTournamentTeam')
export class CreateTournamentTeam {
  @JsonProperty('name', String)
  private _name: string = null;
  @JsonProperty('image', String)
  private _image: string = null;
  @JsonProperty('tournamentId', Number)
  private _tournamentId: number = null;

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get tournamentId(): number {
    return this._tournamentId;
  }

  set tournamentId(value: number) {
    this._tournamentId = value;
  }

  get image(): string {
    return this._image;
  }

  set image(value: string) {
    this._image = value;
  }
}
