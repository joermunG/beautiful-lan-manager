import {JsonObject, JsonProperty} from 'json2typescript';
import {EntityPermission} from '../../enums/entity-permission';
import {TournamentTeamMember} from './tournament-team-member';

@JsonObject('TournamentTeam')
export class TournamentTeam {
  @JsonProperty('id', Number)
  private _id: number = null;
  @JsonProperty('name', String)
  private _name: string = null;

  @JsonProperty('imageUrl', String)
  private _imageUrl: string | null = null;

  @JsonProperty('members', [TournamentTeamMember])
  private _members: TournamentTeamMember[] = [];

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get members(): TournamentTeamMember[] {
    return this._members.sort(
      (a, b) =>
        a.permission === b.permission ? 0 :
          a.permission === EntityPermission.OWNER ? -1 :
            a.permission === EntityPermission.INVITEE ? 1 : 0);
  }

  set members(value: TournamentTeamMember[]) {
    this._members = value;
  }

  get joinedMembers(): TournamentTeamMember[] {
    return this.members.filter(m => m.permission !== EntityPermission.INVITEE);
  }

  get imageUrl(): string | null {
    return this._imageUrl;
  }

  set imageUrl(value: string | null) {
    this._imageUrl = value;
  }
}
