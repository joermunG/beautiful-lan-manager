export class Pagination {
  public page = 1;
  public pageSize = 10;

  constructor(page: number, pageSize: number) {
    this.page = page;
    this.pageSize = pageSize;
  }
}
