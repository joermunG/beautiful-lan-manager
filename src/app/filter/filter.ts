import {AbstractFilter} from './abstract-filter';
import {Pagination} from './pagination';
import {RelationalOperator} from './relational-operator';

export class Filter<T extends AbstractFilter> {
  private readonly filter: T;
  private readonly pagination: Pagination;
  private readonly sort: Map<keyof T, 'ASC' | 'DESC'> = new Map<keyof T, 'ASC' | 'DESC'>();

  constructor(filterClass: new() => T, page = 1, pageSize = 10) {
    this.filter = new filterClass();
    this.pagination = new Pagination(page, pageSize);
  }

  public importFromFilter(filter: T) {
    // TODO implement import
    throw new Error('Not implemented');
  }

  public importFromJsObject() {
    // TODO implement import
    throw new Error('Not implemented');
  }

  public getFilter<K extends keyof T>(key: K): T[K] {
    return this.filter[key];
  }

  public setPage(page: number): Filter<T> {
    this.pagination.page = page;
    return this;
  }

  public setPageSize(pageSize: number): Filter<T> {
    this.pagination.pageSize = pageSize;
    return this;
  }

  public setFilter<K extends keyof T>(
    key: K,
    operator: RelationalOperator,
    value: T[K] extends Map<RelationalOperator, infer V> ? V | V[] : never
  ): Filter<T> {
    const x = this.filter[key];
    if (x instanceof Map) {
      x.set(operator, value);
    }
    return this;
  }

  public setSort(
    key: keyof T,
    value: 'ASC' | 'DESC'
  ): Filter<T> {
    this.sort.set(key, value);
    return this;
  }

  public removeFilter<K extends keyof T>(key: K, operator: RelationalOperator): Filter<T> {
    const x = this.filter[key];
    if (x instanceof Map) {
      x.delete(operator);
    }
    return this;
  }

  private getFilledFilters(): [string, RelationalOperator, any][] {
    const whereTupel: [string, RelationalOperator, any][] = [];
    for (const i in this.filter) {
      if (this.filter.hasOwnProperty(i)) {
        // console.log(i);
        // console.log(this.filter[i]);
        const j = this.filter[i];
        if (j instanceof Map) {
          for (const [operator, value] of j.entries()) {
            // console.log('op: ' + operator);
            // console.log('val: ' + value);
            whereTupel.push([i, operator, value]);
          }
        }
      }
    }
    return whereTupel;
  }

  public getQueryString(limiting: boolean): string {
    const queryParams: string[] = [];
    const queryParamsFilter: string[] = [];
    const queryParamsPagination: string[] = [];
    const queryParamsSort: string[] = [];
    const whereTupel = this.getFilledFilters();
    // create from pagination

    for (const [key, operator, value] of whereTupel) {
      let val = value;

      if (Array.isArray(value)) {
        val = value.join('|');
      }
      queryParamsFilter.push(`${key}${operator}${val}`);
    }
    if (queryParamsFilter.length > 0) {
      queryParams.push('filters=' + queryParamsFilter.join(','));
    }

    for (const i of this.sort.keys()) {
      switch (this.sort.get(i)) {
        case 'ASC':
          queryParamsSort.push(i.toString());
          break;
        case 'DESC':
          queryParamsSort.push('-' + i);
          break;
      }
    }
    if (queryParamsSort.length > 0) {
      queryParams.push('sorts=' + queryParamsSort.join(','));
    }

    if (limiting) {
      queryParamsPagination.push(`page=${this.pagination.page}`);
      queryParamsPagination.push(`pageSize=${this.pagination.pageSize}`);
      queryParams.push(queryParamsPagination.join('&'));
    }

    return queryParams.length > 0 ? '?' + queryParams.join('&') : '';
  }
}
