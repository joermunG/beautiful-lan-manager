export enum RelationalOperator {
  EQUALS = '==',
  NOT_EQUALS = '!=',
  GREATER_THAN = '>',
  GREATER_THAN_OR_EQUAL_TO = '>=',
  LESS_THAN = '<',
  LESS_THAN_OR_EQUAL_TO = '<=',
  CONTAINS = '@=',
  NOT_CONTAINS = '!@=',
  STARTS_WITH = '_=',
  NOT_STARTS_WITH = '!_=',
  CONTAINS_CI = '@=*',
  STARTS_WITH_CI = '_=*',
  EQUALS_CI = '==*',
  NOT_CONTAINS_CI = '!@=*',
  NOT_STARTS_WITH_CI = '!_=*'
}
