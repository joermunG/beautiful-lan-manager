export enum UserRole {
  MANAGER = 'Manager',
  ADMIN = 'Admin',
  USER = 'User'
}
