export enum GridSquareRotation {
  ROTATION_0 = 'Degree0',
  ROTATION_90 = 'Degree90',
  ROTATION_180 = 'Degree180',
  ROTATION_270 = 'Degree270',
}
