export enum EntityPermission {
  OWNER = 'Owner',
  MEMBER = 'Member',
  INVITEE = 'Invitee'
}
