export enum TournamentState {
  PLANNING = 'Planning',
  OPEN_FOR_REGISTRATION = 'OpenForRegistration',
  RUNNING = 'Running',
  FINISHED = 'Finished',
  CLOSED = 'Closed'
}
