export enum TournamentMode {
  SINGLE_ELIMINATION = 'SingleElimination',
  DOUBLE_ELIMINATION = 'DoubleElimination'
}
