import {Component, OnInit} from '@angular/core';
import {NbPosition} from '@nebular/theme';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {User} from '../classes/user/user';
import {Filter} from '../filter/filter';
import {UserFilter} from '../modules/user/api/user-filter';
import {UsersApiService} from '../modules/user/api/users-api.service';

@Component({
  selector: 'lan-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  constructor(private usersApi: UsersApiService) {
  }

  public users$: Observable<User[]>;

  public NbPosition = NbPosition;

  ngOnInit() {
    const userFilter = new Filter<UserFilter>(UserFilter, 1, 25);
    this.users$ = this.usersApi.getFilteredUsers(userFilter).pipe(
      map(resp => resp.results),
      tap(users => console.log(users))
    );
  }

}
