import { TestBed } from '@angular/core/testing';

import { ClearCachesService } from './clear-caches.service';

describe('ClearCachesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClearCachesService = TestBed.get(ClearCachesService);
    expect(service).toBeTruthy();
  });
});
