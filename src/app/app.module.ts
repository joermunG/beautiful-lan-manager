import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NbAuthModule, NbAuthSimpleToken, NbPasswordAuthStrategy} from '@nebular/auth';
import {NbEvaIconsModule} from '@nebular/eva-icons';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbDialogModule,
  NbLayoutModule,
  NbListModule,
  NbMenuModule,
  NbSidebarModule,
  NbSpinnerModule,
  NbThemeModule,
  NbToastrModule,
  NbTooltipModule,
  NbUserModule
} from '@nebular/theme';
import {NgSelectModule} from '@ng-select/ng-select';
import {AppComponent} from './app.component';
import {LayoutComponent} from './layout-component/layout.component';
import {LanChatModule} from './modules/lan-chat/lan-chat.module';
import {LayoutModule} from './modules/layout-module/layout.module';
import {AppRoutingModule} from './modules/router/app-routing.module';
import {UserModule} from './modules/user/user.module';


@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    UserModule,
    LanChatModule,
    FontAwesomeModule,
    NbToastrModule.forRoot(),
    NbThemeModule.forRoot({name: 'dark'}),
    NbLayoutModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbEvaIconsModule,
    NbTooltipModule,
    LayoutModule,
    NbUserModule,
    NbAuthModule.forRoot({
      strategies: [
        NbPasswordAuthStrategy.setup({
          name: 'email',
          token: {
            class: NbAuthSimpleToken,
            key: 'id', // this parameter tells where to look for the token
          },
          baseEndpoint: '/api/v1',
          login: {
            // ...
            endpoint: '/Auth/login',
            method: 'post',
            redirect: {
              success: '/',
              failure: null,
            },
          },
          logout: {
            endpoint: '/Auth/logout',
            method: 'delete',
            redirect: {
              success: '/auth/login'
            }
          },
          resetPass: {
            endpoint: 'Auth/reset-pass',
            method: 'put',
            redirect: {
              success: '/',
              failure: null,
            },
            resetPasswordTokenKey: 'reset_password_token',
            defaultErrors: ['Something went wrong, please try again.'],
            defaultMessages: ['Your password has been successfully changed.'],
          },
        }),
      ],
      forms: {},
    }),
    NbListModule,
    NbActionsModule,
    NbCardModule,
    NbSpinnerModule,
    NbButtonModule,
    NbDialogModule.forRoot(),
    NgSelectModule
  ],
  providers: [],
  exports: [
    LayoutComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
