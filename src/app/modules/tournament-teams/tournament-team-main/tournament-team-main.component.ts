import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {of} from 'rxjs';
import {switchMap, take, tap} from 'rxjs/operators';
import {TournamentTeam} from '../../../classes/tournament-teams/tournament-team';
import {Tournament} from '../../../classes/tournament/tournament';
import {User} from '../../../classes/user/user';
import {CurrentUserApiService} from '../../user/api/current-user-api.service';

@Component({
  selector: 'lan-tournament-team-main',
  templateUrl: './tournament-team-main.component.html',
  styleUrls: ['./tournament-team-main.component.scss']
})
export class TournamentTeamMainComponent implements OnInit {

  constructor(private route: ActivatedRoute, private currentUserApi: CurrentUserApiService) {
  }

  public tournament: Tournament;
  public ownTournamentTeam: TournamentTeam;
  public currentUser: User;

  ngOnInit() {
    console.log(this.route);
    this.tournament = this.route.snapshot.data.tournament;

    this.currentUserApi.currentUser$.pipe(
      take(1),
      tap(cu => this.currentUser = cu),
      switchMap(_ => of(this.tournament)),
      tap(tournament => this.ownTournamentTeam = tournament.getUserTeam(this.currentUser))
    ).subscribe();
  }
}
