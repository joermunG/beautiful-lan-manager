import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentTeamMainComponent } from './tournament-team-main.component';

describe('TournamentTeamMainComponent', () => {
  let component: TournamentTeamMainComponent;
  let fixture: ComponentFixture<TournamentTeamMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TournamentTeamMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentTeamMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
