import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentTeamCardComponent } from './tournament-team-card.component';

describe('TournamentTeamCardComponent', () => {
  let component: TournamentTeamCardComponent;
  let fixture: ComponentFixture<TournamentTeamCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TournamentTeamCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentTeamCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
