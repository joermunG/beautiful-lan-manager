import {Location} from '@angular/common';
import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {Router} from '@angular/router';
import {faCrown} from '@fortawesome/free-solid-svg-icons/faCrown';
import {faEllipsisV} from '@fortawesome/free-solid-svg-icons/faEllipsisV';
import {faSignInAlt} from '@fortawesome/free-solid-svg-icons/faSignInAlt';
import {faUserFriends} from '@fortawesome/free-solid-svg-icons/faUserFriends';
import {NbToastrService} from '@nebular/theme';
import {BehaviorSubject, from, Observable, Subject} from 'rxjs';
import {filter, map, switchMap, take, takeUntil, tap} from 'rxjs/operators';
import {isNull} from 'util';
import {TournamentTeam} from '../../../classes/tournament-teams/tournament-team';
import {TournamentTeamMember} from '../../../classes/tournament-teams/tournament-team-member';
import {Tournament} from '../../../classes/tournament/tournament';
import {User} from '../../../classes/user/user';
import {EntityPermission} from '../../../enums/entity-permission';
import {TournamentTeamApiService} from '../../tournaments/api/tournament-team-api.service';
import {CurrentUserApiService} from '../../user/api/current-user-api.service';

@Component({
  selector: 'lan-tournament-team-card',
  templateUrl: './tournament-team-card.component.html',
  styleUrls: ['./tournament-team-card.component.scss']
})
export class TournamentTeamCardComponent implements OnInit, OnChanges, OnDestroy {

  @Input() tournament: Tournament;
  @Input() tournamentTeam: TournamentTeam;

  constructor(private location: Location,
              private currentUserApi: CurrentUserApiService,
              private router: Router,
              private toast: NbToastrService,
              private tournamentTeamApi: TournamentTeamApiService) {
  }

  public faOwner = faCrown;
  public faMember = faUserFriends;
  public faInvitee = faSignInAlt;
  public faEllipsis = faEllipsisV;
  public EntitityPermission = EntityPermission;

  public currentUser: User;
  public userToInvite: User = null;

  public isOwner$: Observable<boolean>;
  public isMember$: Observable<boolean>;
  public isInvited$: Observable<boolean>;

  private tournamentTeamEvent = new BehaviorSubject<TournamentTeam>(null);
  private unsubscribe = new Subject();

  ngOnInit() {
    this.currentUserApi.currentUser$.pipe(
      take(1),
    ).subscribe(user => this.currentUser = user);

    this.tournamentTeamEvent.pipe(
      takeUntil(this.unsubscribe),
      tap(team => this.tournamentTeam = team)
    ).subscribe();

    this.isOwner$ = this.tournamentTeamEvent.pipe(
      filter((team) => !isNull(team)),
      takeUntil(this.unsubscribe),
      switchMap(team => from(team.members)),
      filter((member) => member.member.id === this.currentUser.id),
      map((ownMember) => ownMember.permission === EntityPermission.OWNER)
    );

    this.isMember$ = this.tournamentTeamEvent.pipe(
      filter((team) => !isNull(team)),
      takeUntil(this.unsubscribe),
      switchMap(team => from(team.members)),
      filter((member) => member.member.id === this.currentUser.id),
      map((ownMember) => ownMember.permission === EntityPermission.MEMBER)
    );

    this.isInvited$ = this.tournamentTeamEvent.pipe(
      filter((team) => !isNull(team)),
      takeUntil(this.unsubscribe),
      switchMap(team => from(team.members)),
      filter((member) => member.member.id === this.currentUser.id),
      map((ownMember) => ownMember.permission === EntityPermission.INVITEE)
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('tournamentTeam') && changes.tournamentTeam.currentValue) {
      this.tournamentTeamEvent.next(changes.tournamentTeam.currentValue);
    }
  }

  public invitePlayer(user: User) {
    this.tournamentTeamApi.invite(this.tournamentTeam, user).pipe(
      take(1),
      tap(updatedTeam => this.tournamentTeamEvent.next(updatedTeam)),
      tap(_ => this.toast.success('You invited ' + user.nickname, 'Invitation sent')),
      tap(_ => this.userToInvite = null)
    ).subscribe();
  }

  public joinTeam(user: User) {
    this.tournamentTeamApi.join(this.tournamentTeam, user).pipe(
      take(1),
      tap(team => this.toast.success('You joined Team ' + team.name, 'Welcome!')),
      tap(updatedTeam => this.tournamentTeamEvent.next(updatedTeam))
    ).subscribe();
  }

  public leaveTeam(user: User) {
    this.tournamentTeamApi.leave(this.tournamentTeam, user).pipe(
      take(1),
      tap(_ => {
        this.toast.success('You left your Team', 'Goodbye!');
      }),
      tap(updatedTeam => {
        if (!updatedTeam) {
          this.router.navigate(['/tournaments']);
        }
        this.tournamentTeamEvent.next(updatedTeam);
      })
    ).subscribe();
  }

  public revokeInvitation(user: User) {
    this.tournamentTeamApi.revokeInvitation(this.tournamentTeam, user).pipe(
      take(1),
      tap(_ => this.toast.success('You revoked ' + user.nickname + '\'s invitation', 'Success')),
      tap(updatedTeam => this.tournamentTeamEvent.next(updatedTeam))
    ).subscribe();
  }

  public grantLeader(user: User) {
    this.tournamentTeamApi.grantLeader(this.tournamentTeam, user).pipe(
      take(1),
      tap(_ => this.toast.success('You promoted ' + user.nickname + ' to lead your Team', 'Success')),
      tap(updatedTeam => this.tournamentTeamEvent.next(updatedTeam))
    ).subscribe();
  }

  public revokeLeader(user: User) {
    this.tournamentTeamApi.revokeLeader(this.tournamentTeam, user).pipe(
      take(1),
      tap(_ => this.toast.success('You revoked ' + user.nickname + '\'s Leader permission', 'Success')),
      tap(updatedTeam => this.tournamentTeamEvent.next(updatedTeam))
    ).subscribe();
  }

  public deleteTeam() {
    this.tournamentTeamApi.deleteTeam(this.tournamentTeam).pipe(
      take(1),
      tap(_ => this.toast.success('You deleted ' + this.tournamentTeam.name, 'Success')),
      tap(_ => this.router.navigate(['/tournaments']))
    ).subscribe();
  }

  public prepareImgUrl(slug: string): string {
    if (slug) {
      return this.location.prepareExternalUrl(slug);
    }
    return null;
  }

  public asMember(member: any): TournamentTeamMember {
    return member as TournamentTeamMember;
  }

  ngOnDestroy() {
    this.unsubscribe.next();
  }

}
