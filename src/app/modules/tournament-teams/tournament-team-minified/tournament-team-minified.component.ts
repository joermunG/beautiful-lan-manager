import {Location} from '@angular/common';
import {Component, Input, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {TournamentTeam} from '../../../classes/tournament-teams/tournament-team';
import {Tournament} from '../../../classes/tournament/tournament';

@Component({
  selector: 'lan-tournament-team-minified',
  templateUrl: './tournament-team-minified.component.html',
  styleUrls: ['./tournament-team-minified.component.scss']
})
export class TournamentTeamMinifiedComponent implements OnInit, OnDestroy {

  @Input() tournament: Tournament;
  @Input() tournamentTeam: TournamentTeam;
  @Input() size: 'tiny' | 'small' | 'medium' | 'large' | 'giant' = 'medium';

  constructor(private location: Location, private dialog: NbDialogService) {
  }

  public modalRef: NbDialogRef<any>;
  private unsubscribe = new Subject();

  ngOnInit() {
  }

  public prepareImgUrl(slug: string): string {
    if (slug) {
      return this.location.prepareExternalUrl(slug);
    }
    return null;
  }

  public openModal(modal: TemplateRef<any>) {
    this.modalRef = this.dialog.open(modal);
    this.modalRef.onClose.pipe(
      takeUntil(this.unsubscribe)
    ).subscribe(_ => this.modalRef = undefined);
  }

  ngOnDestroy() {
    this.unsubscribe.next();
  }

}
