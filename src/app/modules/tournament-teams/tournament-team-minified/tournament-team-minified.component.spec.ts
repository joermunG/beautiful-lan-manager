import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentTeamMinifiedComponent } from './tournament-team-minified.component';

describe('TournamentTeamMinifiedComponent', () => {
  let component: TournamentTeamMinifiedComponent;
  let fixture: ComponentFixture<TournamentTeamMinifiedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TournamentTeamMinifiedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentTeamMinifiedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
