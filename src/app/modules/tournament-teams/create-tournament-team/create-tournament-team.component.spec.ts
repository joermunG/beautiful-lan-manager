import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTournamentTeamComponent } from './create-tournament-team.component';

describe('CreateTournamentTeamComponent', () => {
  let component: CreateTournamentTeamComponent;
  let fixture: ComponentFixture<CreateTournamentTeamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTournamentTeamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTournamentTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
