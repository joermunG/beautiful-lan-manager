import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NbComponentStatus} from '@nebular/theme';
import {Subject} from 'rxjs';
import {takeUntil, tap} from 'rxjs/operators';
import {CreateTournamentTeam} from '../../../classes/tournament-teams/create-tournament-team';
import {Tournament} from '../../../classes/tournament/tournament';
import {CurrentUserApiService} from '../../user/api/current-user-api.service';

@Component({
  selector: 'lan-create-tournament-team',
  templateUrl: './create-tournament-team.component.html',
  styleUrls: ['./create-tournament-team.component.scss']
})
export class CreateTournamentTeamComponent implements OnInit, OnChanges, OnDestroy {

  @Input() tournament: Tournament;
  @Output() save: EventEmitter<CreateTournamentTeam> = new EventEmitter<CreateTournamentTeam>();
  @Output() cancel: EventEmitter<any> = new EventEmitter<any>();

  constructor(private currentUser: CurrentUserApiService) {
  }

  public currentUser$ = this.currentUser.currentUser$;

  public form = new FormGroup({
    name: new FormControl(null,
      {
        validators: Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(100)]),
        updateOn: 'blur'
      }
    ),
    image: new FormControl(null, Validators.compose([Validators.required])),
    tournamentId: new FormControl(null)
  });
  public fileConfirmationEvent: EventEmitter<string> = new EventEmitter<string>();

  private unsubscribe: Subject<any> = new Subject<any>();

  ngOnInit() {
    this.fileConfirmationEvent.pipe(
      takeUntil(this.unsubscribe.asObservable()),
      tap(img => {
        this.form.get('image').patchValue(img);
        this.form.get('image').markAsTouched();
      })
    ).subscribe();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('tournament') && changes.tournament.currentValue) {
      const t: Tournament = changes.tournament.currentValue;
      this.form.patchValue({
        name: null,
        tournamentId: t.id,
        image: null
      });
    }
  }

  public submit() {
    if (this.form.valid) {
      const payload: CreateTournamentTeam = Object.assign(new CreateTournamentTeam(), this.form.getRawValue());
      this.save.emit(payload);
    }
  }

  public formFieldStatus(fieldName: string): NbComponentStatus {
    const field = this.form.get(fieldName);
    if (!field.touched) {
      return null;
    }
    if (field.touched && field.invalid) {
      return 'danger';
    }
    if (field.touched && field.valid) {
      return 'primary';
    }
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
