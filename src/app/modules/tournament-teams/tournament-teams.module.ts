import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NbButtonModule, NbCardModule, NbInputModule, NbPopoverModule, NbTooltipModule, NbUserModule} from '@nebular/theme';
import {LayoutModule} from '../layout-module/layout.module';
import {UserModule} from '../user/user.module';
import {CreateTournamentTeamComponent} from './create-tournament-team/create-tournament-team.component';
import {TournamentTeamCardComponent} from './tournament-team-card/tournament-team-card.component';
import {TournamentTeamMainComponent} from './tournament-team-main/tournament-team-main.component';
import {TournamentTeamMinifiedComponent} from './tournament-team-minified/tournament-team-minified.component';
import {TournamentTeamComponent} from './tournament-team/tournament-team.component';


@NgModule({
  declarations: [
    CreateTournamentTeamComponent,
    TournamentTeamMainComponent,
    TournamentTeamCardComponent,
    TournamentTeamComponent,
    TournamentTeamMinifiedComponent
  ],
  imports: [
    CommonModule,
    NbCardModule,
    LayoutModule,
    ReactiveFormsModule,
    NbInputModule,
    NbButtonModule,
    NbUserModule,
    UserModule,
    FontAwesomeModule,
    NbTooltipModule,
    NbPopoverModule,
    RouterModule
  ],
  exports: [
    CreateTournamentTeamComponent,
    TournamentTeamCardComponent,
    TournamentTeamComponent,
    TournamentTeamMinifiedComponent
  ]
})
export class TournamentTeamsModule {
}
