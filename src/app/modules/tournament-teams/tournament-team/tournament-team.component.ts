import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TournamentTeam} from '../../../classes/tournament-teams/tournament-team';
import {Tournament} from '../../../classes/tournament/tournament';

@Component({
  selector: 'lan-tournament-team',
  templateUrl: './tournament-team.component.html',
  styleUrls: ['./tournament-team.component.scss']
})
export class TournamentTeamComponent implements OnInit {

  @Input() team: TournamentTeam;
  @Input() tournament: Tournament;
  @Output() teamHover: EventEmitter<TournamentTeam> = new EventEmitter<TournamentTeam>();

  constructor() {
  }

  ngOnInit() {
  }

  public onTeamMouseEnter(event: MouseEvent, team: TournamentTeam) {
    this.teamHover.emit(team);
  }

  public onTeamMouseLeave() {
    this.teamHover.emit(null);
  }
}
