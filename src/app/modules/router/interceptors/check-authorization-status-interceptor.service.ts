import {HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NbTokenStorage} from '@nebular/auth';
import {tap} from 'rxjs/operators';
import {ClearCachesService} from '../../../clear-caches.service';
import {CurrentUserApiService} from '../../user/api/current-user-api.service';

@Injectable()
export class CheckAuthorizationStatusInterceptor implements HttpInterceptor {
  constructor(private currentUser: CurrentUserApiService,
              private clearCaches: ClearCachesService,
              private tokenStorage: NbTokenStorage,
              private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {

    // extend server response observable with logging
    return next.handle(req)
      .pipe(
        tap(
          // Succeeds when there is a response; ignore other events
          event => event,
          // Operation failed; error is an HttpErrorResponse
          () => {
            this.currentUser.isAuthorized$().pipe(
              tap(isAuthorized => {
                if (!isAuthorized) {
                  this.clearCaches.clearCachesEvent.next();
                  this.tokenStorage.clear();
                  this.router.navigate(['/auth/login']);
                }
              })
            ).subscribe();
          }
        )
      );
  }
}
