import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {Room} from '../../../classes/room/room';
import {RoomsApiService} from '../../rooms/api/rooms-api.service';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RoomResolverService implements Resolve<Room> {
  constructor(private roomsApi: RoomsApiService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Room> | Observable<never> {
    return this.roomsApi.getRoomById(route.params.id).pipe(
      catchError(e => {
        return of(null).pipe(
          tap(() => this.router.navigate(['404']))
        );
      })
    );
  }
}
