import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {User} from '../../../classes/user/user';
import {UsersApiService} from '../../user/api/users-api.service';

@Injectable({
  providedIn: 'root'
})
export class UserResolverService implements Resolve<User> {
  constructor(private userApi: UsersApiService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> | Observable<never> {
    let id = +route.paramMap.get('id');

    // navigated to child of /user -> get params from parent
    // TODO check this
    if (id === 0) {
      id = +route.parent.paramMap.get('id');
    }

    return this.userApi.getUserById(id);
  }
}
