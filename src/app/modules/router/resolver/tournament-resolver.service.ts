import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {Tournament} from '../../../classes/tournament/tournament';
import {TournamentsApiService} from '../../tournaments/api/tournaments-api.service';

@Injectable({
  providedIn: 'root'
})
export class TournamentResolverService implements Resolve<Tournament> {

  constructor(private tournamentsApi: TournamentsApiService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Tournament> | Observable<never> {
    const tournamentId: number = +route.paramMap.get('tournamentId');

    if (!tournamentId) {
      this.router.navigate(['tournaments', '404']);
      return;
    }
    return this.tournamentsApi.getTournamentById(tournamentId).pipe(
      catchError(() => {
        return of(null).pipe(
          tap(() => this.router.navigate(['tournaments', '404']))
        );
      })
    );
  }
}
