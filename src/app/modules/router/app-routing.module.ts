import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NbAuthComponent, NbLoginComponent, NbLogoutComponent, NbResetPasswordComponent} from '@nebular/auth';
import {LayoutComponent} from '../../layout-component/layout.component';
import {PageForbiddenComponent} from '../layout-module/page-forbidden/page-forbidden.component';
import {PageNotFoundComponent} from '../layout-module/page-not-found/page-not-found.component';
import {NewRoomComponent} from '../rooms/new-room/new-room.component';
import {RoomsMainComponent} from '../rooms/rooms-main/rooms-main.component';
import {TournamentTeamMainComponent} from '../tournament-teams/tournament-team-main/tournament-team-main.component';
import {NewTournamentComponent} from '../tournaments/new-tournament/new-tournament.component';
import {TournamentRegistrationComponent} from '../tournaments/tournament-registration/tournament-registration.component';
import {TournamentDetailsComponent} from '../tournaments/tournaments-main/tournament-details/tournament-details.component';
import {TournamentsMainComponent} from '../tournaments/tournaments-main/tournaments-main.component';
import {UserEditComponent} from '../user/user-main/user-edit/user-edit.component';
import {UserMainComponent} from '../user/user-main/user-main.component';
import {UserProfileComponent} from '../user/user-main/user-profile/user-profile.component';
import {AuthGuard} from './guards/auth-guard.service';
import {CanSeeProfileDetailsGuardService} from './guards/can-see-profile-details-guard.service';
import {ManagerGuard} from './guards/manager-guard.service';
import {CheckAuthorizationStatusInterceptor} from './interceptors/check-authorization-status-interceptor.service';
import {TournamentResolverService} from './resolver/tournament-resolver.service';
import {UserResolverService} from './resolver/user-resolver.service';


const routes: Routes = [
  {
    path: '', component: LayoutComponent, canActivate: [AuthGuard], children: [
      {
        path: 'user/:id',
        component: UserMainComponent, resolve: {user: UserResolverService}, runGuardsAndResolvers: 'always',
        children: [
          {
            path: 'profile',
            component: UserProfileComponent, resolve: {user: UserResolverService}
          },
          {
            path: 'edit',
            component: UserEditComponent, canActivate: [CanSeeProfileDetailsGuardService],
            resolve: {user: UserResolverService}, runGuardsAndResolvers: 'always',
          }
        ]
      },
      {
        path: 'tournaments', component: TournamentsMainComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        path: 'tournaments/new',
        component: NewTournamentComponent, canActivate: [ManagerGuard]
      },
      {
        path: 'tournaments/:tournamentId',
        resolve: {tournament: TournamentResolverService},
        component: TournamentDetailsComponent
      },
      {
        path: 'tournaments/:tournamentId/register',
        component: TournamentRegistrationComponent,
        resolve: {tournament: TournamentResolverService}
      },
      {
        path: 'tournaments/:tournamentId/teams',
        component: TournamentTeamMainComponent,
        resolve: {tournament: TournamentResolverService}
      },
      {path: 'tournaments/404', component: PageNotFoundComponent},
      {path: 'tournaments/**', redirectTo: 'tournaments/404'},
      {
        path: 'rooms', component: RoomsMainComponent,
        runGuardsAndResolvers: 'always',
      },
      {
        path: 'rooms/new',
        component: NewRoomComponent, canActivate: [ManagerGuard]
      },
      // { TODO Add edit room route and component
      //   path: 'rooms/:id', component: RoomsDetailsComponent, resolve: {room: RoomResolverService}
      // },
    ]
  }, {
    path: 'auth',
    component: NbAuthComponent,
    children: [
      {
        path: '',
        component: NbLoginComponent,
      },
      {
        path: 'login',
        component: NbLoginComponent,
      },
      {
        path: 'logout',
        component: NbLogoutComponent,
      },
      // {
      //   path: 'register',
      //   component: NbRegisterComponent,
      // },
      // {
      //   path: 'request-password',
      //   component: NbRequestPasswordComponent,
      // },
      {
        path: 'reset-password',
        component: NbResetPasswordComponent,
      },
    ],
  },
  {path: '404', component: PageNotFoundComponent},
  {path: '403', component: PageForbiddenComponent},
  {path: '**', redirectTo: '404'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing: false})],
  exports: [RouterModule],
  providers: [
    AuthGuard,
    ManagerGuard,
    CanSeeProfileDetailsGuardService,
    UserResolverService,
    TournamentResolverService,
    {provide: HTTP_INTERCEPTORS, useClass: CheckAuthorizationStatusInterceptor, multi: true}
  ]
})
export class AppRoutingModule {
}
