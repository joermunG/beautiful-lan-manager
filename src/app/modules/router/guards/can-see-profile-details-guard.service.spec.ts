import {TestBed} from '@angular/core/testing';

import {CanSeeProfileDetailsGuardService} from './can-see-profile-details-guard.service';

describe('CanSeeProfileDetailsGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CanSeeProfileDetailsGuardService = TestBed.get(CanSeeProfileDetailsGuardService);
    expect(service).toBeTruthy();
  });
});
