import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {take, tap} from 'rxjs/operators';
import {UserRole} from '../../../enums/user-role';
import {CurrentUserApiService} from '../../user/api/current-user-api.service';

@Injectable({
  providedIn: 'root'
})
export class ManagerGuard implements CanActivate {

  constructor(private currentUserApi: CurrentUserApiService, private router: Router) {
  }

  canActivate() {
    return this.currentUserApi.currentUserHasRole$(UserRole.MANAGER)
      .pipe(
        take(1),
        tap(isManager => {
          if (!isManager) {
            this.router.navigate(['403'], {skipLocationChange: true});
          }
        }),
      );
  }
}
