import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {of} from 'rxjs';
import {switchMap, tap} from 'rxjs/operators';
import {UserRole} from '../../../enums/user-role';
import {CurrentUserApiService} from '../../user/api/current-user-api.service';

@Injectable({
  providedIn: 'root'
})
export class CanSeeProfileDetailsGuardService implements CanActivate {

  constructor(private currentUserApi: CurrentUserApiService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot) {
    // navigated to child of /user -> get params from parent
    const id = +route.parent.paramMap.get('id');

    return this.currentUserApi.isCurrentUser$(id)
      .pipe(
        switchMap(isUser => {
          return isUser ? of(isUser) : this.currentUserApi.currentUserHasRole$(UserRole.ADMIN);
        }),
        tap(isAuthorized => {
          if (!isAuthorized) {
            this.router.navigate(['403']);
          }
        })
      );
  }
}
