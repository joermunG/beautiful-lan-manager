import {AbstractFilter} from '../../../filter/abstract-filter';
import {RelationalOperator} from '../../../filter/relational-operator';

export class LanEventFilter extends AbstractFilter {
  public id: Map<RelationalOperator, number | number[]> = new Map<RelationalOperator, number | number[]>();
}
