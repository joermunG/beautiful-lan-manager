import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, ReplaySubject, Subject, throwError} from 'rxjs';
import {catchError, map, shareReplay} from 'rxjs/operators';
import {LanEvent} from '../../../classes/lan-event/lan-event';
import {AbstractApiService} from '../../../abstract-services/abstract-api.service';
import {Filter} from '../../../filter/filter';
import {PagedResult} from '../../../classes/paged-result';
import {LanEventFilter} from './lan-event-filter';
import {RelationalOperator} from '../../../filter/relational-operator';
import {CreateLanEvent} from '../../../classes/lan-event/create-lan-event';
import {NbToastrService} from '@nebular/theme';
import {UpdateLanEvent} from '../../../classes/lan-event/update-lan-event';

@Injectable({
  providedIn: 'root'
})
export class EventsApiService extends AbstractApiService<LanEvent> {

  baseUrl = '/api/v1/Events';

  private currentEvent: ReplaySubject<LanEvent> = new ReplaySubject<LanEvent>(1);
  public currentEvent$: Observable<LanEvent> = this.currentEvent.asObservable();

  constructor(public http: HttpClient, private toast: NbToastrService) {
    super(LanEvent);
    const filter = new Filter(LanEventFilter, 1, 1);
    filter.setSort('id', 'DESC');
    this.getFilteredEvents(filter).pipe(
      map(pr => {
        if (pr.rowCount !== 1) {
          throwError(new Error('There is no LanEvent in database'));
        } else {
          return pr.results[0];
        }
      })
    ).subscribe((ev: LanEvent) => this.currentEvent.next(ev));
  }

  public getLanEventById(id: number): Observable<LanEvent> {
    const filter = new Filter(LanEventFilter, 1, 1);
    filter.setFilter('id', RelationalOperator.EQUALS, id);
    return this.getFiltered(this.baseUrl, filter).pipe(
      map(r => {
        if (r.rowCount !== 1) {
          throwError(new Error('LanEvent with id ' + id + ' not found'));
        } else {
          return r;
        }
      }),
      map(pagedResult => pagedResult.results.shift())
    );
  }

  public getFilteredEvents(filter: Filter<LanEventFilter>): Observable<PagedResult<LanEvent>> {
    return this.getFiltered(this.baseUrl, filter);
  }

  public createLanEvent(createMessage: CreateLanEvent): Observable<LanEvent> {
    return this.postGeneric(this.baseUrl, this.jsonConvert.deserializeObject(createMessage, CreateLanEvent), LanEvent).pipe(
      catchError((err) => {
        this.toast.danger(createMessage.name, 'Creation Failed!');
        return throwError(err);
      })
    );
  }

  public updateLanEvent(lanEventId: number, updateMessage: UpdateLanEvent): Observable<LanEvent> {
    return this.putGeneric(
      this.baseUrl + '/' + lanEventId,
      this.jsonConvert.deserializeObject(updateMessage, UpdateLanEvent), LanEvent).pipe(
      catchError((err) => {
        this.toast.danger(lanEventId, 'Update Failed!');
        return throwError(err);
      })
    );
  }

  public deleteRoom(lanEventId: number): Observable<null> {
    return super.deleteGeneric<null>(this.baseUrl + '/' + lanEventId, null).pipe(
      catchError((err) => {
        this.toast.danger(lanEventId, 'Deletion of LanEvent Failed!');
        return throwError(err);
      })
    );
  }

  public joinLanEvent(lanEventId: number, userId: number): Observable<LanEvent> {
    return this.postGeneric(this.baseUrl + '/' + lanEventId + '/join', {userId}, LanEvent);
  }

  public leaveLanEvent(lanEventId: number, userId: number): Observable<LanEvent> {
    return this.postGeneric(this.baseUrl + '/' + lanEventId + '/leave', {userId}, LanEvent);
  }
}
