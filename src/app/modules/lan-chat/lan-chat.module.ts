import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {NbChatModule} from '@nebular/theme';
import {ChatMainComponent} from './chat-main/chat-main.component';


@NgModule({
  declarations: [
    ChatMainComponent
  ],
  exports: [
    ChatMainComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NbChatModule
  ]
})
export class LanChatModule {
}
