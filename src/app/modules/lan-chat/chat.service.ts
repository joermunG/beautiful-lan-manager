import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {User} from '../../classes/user/user';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor() {
  }

  private openChats: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);
  public openChats$: Observable<User[]> = of([]);

  public open(user: User) {
    const newChats: User[] = this.openChats.getValue().concat([user]);
    this.openChats.next(newChats);
  }

  public close(user: User) {
    const newChats: User[] = this.openChats.getValue().filter(u => u.id !== user.id);
    this.openChats.next(newChats);
  }

}
