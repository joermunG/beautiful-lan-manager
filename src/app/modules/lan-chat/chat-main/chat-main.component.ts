import {Component, OnInit} from '@angular/core';
import {take, tap} from 'rxjs/operators';
import {UsersApiService} from '../../user/api/users-api.service';
import {ChatService} from '../chat.service';

@Component({
  selector: 'lan-chat-main',
  templateUrl: './chat-main.component.html',
  styleUrls: ['./chat-main.component.scss']
})
export class ChatMainComponent implements OnInit {

  constructor(private chatService: ChatService, private userService: UsersApiService) {
  }

  public openChats$ = this.chatService.openChats$;

  ngOnInit() {
  }

  public openChat() {
    this.userService.getUserById(2).pipe(
      take(1),
      tap(u => this.chatService.open(u))
    ).subscribe();
  }

}
