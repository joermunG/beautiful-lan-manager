import {Location} from '@angular/common';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Observable, of} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';
import {User} from '../../../../classes/user/user';
import {CurrentUserApiService} from '../../api/current-user-api.service';

@Component({
  selector: 'lan-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private currentUserApi: CurrentUserApiService,
              private location: Location) {
  }

  public user$: Observable<User>;

  public isCurrentUser$: Observable<boolean>;

  ngOnInit() {
    this.user$ = this.route.data.pipe(
      map(data => {
        return data.hasOwnProperty('user') ? (data.user as User) : null;
      })
    );

    this.isCurrentUser$ = this.user$.pipe(
      switchMap(user => {
        return user ? this.currentUserApi.isCurrentUser$(user.id) : of(false);
      })
    );
  }


  public prepareImgUrl(slug: string): string {
    if (slug) {
      return this.location.prepareExternalUrl(slug);
    }
    return null;
  }
}
