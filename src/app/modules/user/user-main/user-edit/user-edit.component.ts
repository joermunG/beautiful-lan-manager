import {Location} from '@angular/common';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {NbComponentStatus, NbToastrService} from '@nebular/theme';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {filter, map, switchMap, take, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {isNull} from 'util';
import {UpdateUser} from '../../../../classes/user/update-user';
import {User} from '../../../../classes/user/user';
import {UsersApiService} from '../../api/users-api.service';

@Component({
  selector: 'lan-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit, OnDestroy {

  constructor(private route: ActivatedRoute,
              private userApi: UsersApiService,
              private toast: NbToastrService,
              private router: Router,
              private location: Location) {
  }

  private userChangeEvent: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  public user$: Observable<User> = this.userChangeEvent.asObservable().pipe(filter(user => !isNull(user)));
  public imageChangeEvent: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  public passwordForm = new FormGroup({
    pw: new FormControl(null, Validators.compose([Validators.minLength(8), Validators.maxLength(255), Validators.required])),
    pwConfirm: new FormControl(null, Validators.compose([Validators.required]))
  }, {updateOn: 'blur', validators: this.passwordsMatchValidator()});

  public userForm = new FormGroup({
    nickname: new FormControl(null, Validators.compose([Validators.minLength(3), Validators.maxLength(30), Validators.required])),
    firstName: new FormControl(null, Validators.compose([Validators.minLength(3), Validators.maxLength(30), Validators.required])),
    lastName: new FormControl(null, Validators.compose([Validators.minLength(3), Validators.maxLength(30), Validators.required]))
  });

  private unsubscribe: Subject<any> = new Subject<any>();

  ngOnInit() {
    this.route.data.pipe(
      take(1),
      map(data => {
        return data.hasOwnProperty('user') ? (data.user as User) : null;
      }), tap(user => {
        this.userChangeEvent.next(user);
        this.userForm.patchValue({
          nickname: user.nickname,
          firstName: user.firstName,
          lastName: user.lastName
        });
      })
    ).subscribe();

    this.imageChangeEvent.asObservable().pipe(
      takeUntil(this.unsubscribe.asObservable()),
      filter(img => !isNull(img)),
      tap(() => {
        this.userForm.markAsDirty();
      })
    ).subscribe();
  }

  public updatePassword() {
    this.user$.pipe(
      take(1),
      switchMap(user => {
        return this.userApi.updatePassword(user.id, this.passwordForm.get('pw').value);
      }), tap(isSuccess => {
        if (isSuccess) {
          this.passwordForm.reset();
          this.passwordForm.markAsPristine();
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate(['../edit'], {relativeTo: this.route});
        }
      })
    ).subscribe();
  }

  public updateUser() {
    const payload: UpdateUser = Object.assign(new UpdateUser(), this.userForm.getRawValue());
    this.user$.pipe(
      take(1),
      withLatestFrom(this.imageChangeEvent.asObservable().pipe(take(1))),
      switchMap(([user, imageString]) => {
        payload.image = imageString;
        return this.userApi.updateUser(user.id, payload);
      }),
      filter(user => !isNull(user)),
      tap(user => {
        this.toast.success('Profile Updated.', 'Success!');
        this.userForm.markAsPristine();
        this.userForm.markAsUntouched();
      })
    ).subscribe();
  }

  public formFieldStatus(formGroup: FormGroup, fieldName: string): NbComponentStatus {
    const field = formGroup.get(fieldName);
    if (!field.touched) {
      return null;
    }
    if (field.touched && field.invalid) {
      return 'danger';
    }
    if (field.touched && field.valid) {
      return 'primary';
    }
  }

  public prepareImgUrl(slug: string): string {
    if (slug) {
      return this.location.prepareExternalUrl(slug);
    }
    return null;
  }

  passwordsMatchValidator(): ValidatorFn {
    return (control: AbstractControl): { passwordsNoMatch: boolean } | null => {
      const noMatch = control.get('pw').value !== control.get('pwConfirm').value;
      return noMatch ? {passwordsNoMatch: true} : null;
    };
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
