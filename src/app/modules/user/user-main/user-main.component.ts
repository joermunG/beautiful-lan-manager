import {Location} from '@angular/common';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, of} from 'rxjs';
import {switchMap, tap} from 'rxjs/operators';
import {UserRole} from '../../../enums/user-role';
import {CurrentUserApiService} from '../api/current-user-api.service';

@Component({
  selector: 'lan-user-main',
  templateUrl: './user-main.component.html',
  styleUrls: ['./user-main.component.scss']
})
export class UserMainComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private currentUserApi: CurrentUserApiService,
              private router: Router,
              private location: Location) {
  }

  public isCurrentUser$: Observable<boolean>;
  public isAdmin$: Observable<boolean> = this.currentUserApi.currentUserHasRole$(UserRole.ADMIN);
  public tabs: any[] = [
    {
      title: 'Your Profile',
      icon: 'person-outline',
      route: './profile',
    },
    {
      title: 'Transaction History',
      icon: 'credit-card-outline',
      route: ['./tab2'],
      disabled: true
    },
    {
      title: 'Edit Profile',
      icon: 'edit-2-outline',
      route: './edit',
    },
  ];

  ngOnInit() {
    this.isCurrentUser$ = this.route.data.pipe(
      switchMap(data => {
        return data && data.user ? this.currentUserApi.isCurrentUser$(data.user.id) : of(false);
      }),
      tap(() => {
        const url = this.location.path().split('/');
        if (url.length === 3) {
          this.router.navigate(['profile'], {relativeTo: this.route});
        }
      })
    );
  }

}
