import {Location} from '@angular/common';
import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {filter, map, tap, withLatestFrom} from 'rxjs/operators';
import {isNull} from 'util';
import {User} from '../../../classes/user/user';
import {CurrentUserApiService} from '../api/current-user-api.service';
import {DisplayImageComponent} from '../helper-classes/DisplayImageComponent';

export enum UserMenuAction {
  CHAT = 'chat',
  PROFILE = 'profile',
  LOGOUT = 'logout'
}

@Component({
  selector: 'lan-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent extends DisplayImageComponent implements OnInit, OnChanges {

  @Input() private user: User;
  @Input() hasFooter = false;
  @Output() menuAction: EventEmitter<UserMenuAction> = new EventEmitter<UserMenuAction>();

  constructor(private currentUserApi: CurrentUserApiService, location: Location) {
    super(location);
  }

  public isCurrentUser = false;
  public UserMenuAction = UserMenuAction;

  private imageChangeEvent: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  public image$ = this.imageChangeEvent.asObservable().pipe(
    filter(img => !isNull(img)),
    map(img => {
      return this.prepareImgUrl(img);
    })
  );

  private userChangeEvent: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  public user$: Observable<User> = this.userChangeEvent.asObservable().pipe(
    filter(u => !isNull(u)),
    tap(user => this.imageChangeEvent.next(user.imageUrl)),
    withLatestFrom(this.currentUserApi.currentUser$),
    tap(([user, currentUser]: [User, User]) => this.isCurrentUser = user.id === currentUser.id),
    map(([user, _]) => user)
  );

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('user') && changes.user.currentValue) {
      this.userChangeEvent.next(changes.user.currentValue);
    }
  }

}
