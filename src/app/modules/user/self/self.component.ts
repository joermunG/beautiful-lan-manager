import {Component, OnInit} from '@angular/core';
import {CurrentUserApiService} from '../api/current-user-api.service';

@Component({
  selector: 'lan-self',
  templateUrl: './self.component.html',
  styleUrls: ['./self.component.scss']
})
export class SelfComponent implements OnInit {

  constructor(private currentUserApi: CurrentUserApiService) {
  }

  public user$;

  ngOnInit() {
    this.user$ = this.currentUserApi.currentUser$;
  }
}
