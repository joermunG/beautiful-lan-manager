import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {filter, map, switchMap, takeUntil, tap} from 'rxjs/operators';
import {isNull} from 'util';
import {User} from '../../../classes/user/user';
import {Filter} from '../../../filter/filter';
import {UserFilter} from '../api/user-filter';
import {UsersApiService} from '../api/users-api.service';

@Component({
  selector: 'lan-user-selector',
  templateUrl: './user-selector.component.html',
  styleUrls: ['./user-selector.component.scss']
})
export class UserSelectorComponent implements OnInit, OnChanges, OnDestroy {
  @Input() selectedUser: User;
  @Output() selectedUserChange: EventEmitter<User> = new EventEmitter<User>();

  constructor(private usersApi: UsersApiService) {
  }

  public selectedUserModel: User = null;

  public currentPage = 1;
  private maxPage: number;

  public loadMoreUsersEvent: BehaviorSubject<number> = new BehaviorSubject<number>(this.currentPage);

  private userList: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);
  public users$: Observable<User[]> = this.userList.asObservable().pipe(
    filter(users => !isNull(users) && users.length > 0),
  );

  private unsubscribe = new Subject();

  ngOnInit() {
    this.loadMoreUsersEvent.asObservable().pipe(
      takeUntil(this.unsubscribe),
      filter(currentPage => !this.maxPage || this.maxPage >= currentPage),
      tap(_ => this.currentPage++),
      switchMap(currentPage => {
        return this.usersApi.getFilteredUsers(new Filter<UserFilter>(UserFilter, currentPage, 15));
      }),
      tap(pagedUsers => this.maxPage = pagedUsers.pageCount),
      map(pagedUsers => pagedUsers.results),
      tap(users => this.userList.next([...this.userList.getValue(), ...users])),
    ).subscribe();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.selectedUserModel = this.selectedUser;
  }

  public onNgModelChange(user: User) {
    this.selectedUserChange.emit(user);
    this.selectedUserModel = user;
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
  }

}
