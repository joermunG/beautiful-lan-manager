import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbListModule,
  NbMenuModule,
  NbPopoverModule,
  NbRouteTabsetModule,
  NbUserModule
} from '@nebular/theme';
import {NgSelectModule} from '@ng-select/ng-select';
import {LayoutModule} from '../layout-module/layout.module';
import {SelfComponent} from './self/self.component';
import {UserCardComponent} from './user-card/user-card.component';
import {UserEditComponent} from './user-main/user-edit/user-edit.component';
import {UserMainComponent} from './user-main/user-main.component';
import {UserProfileComponent} from './user-main/user-profile/user-profile.component';
import {UserSelectorComponent} from './user-selector/user-selector.component';
import {UserComponent} from './user/user.component';


@NgModule({
  declarations: [
    UserComponent,
    SelfComponent,
    UserProfileComponent,
    UserMainComponent,
    UserEditComponent,
    UserSelectorComponent,
    UserCardComponent
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    NbUserModule,
    NbCardModule,
    NbPopoverModule,
    NbMenuModule,
    NbIconModule,
    RouterModule,
    NbActionsModule,
    NbRouteTabsetModule,
    NbListModule,
    NbInputModule,
    ReactiveFormsModule,
    NbButtonModule,
    LayoutModule,
    FormsModule,
    NgSelectModule
  ],
  exports: [
    UserComponent,
    SelfComponent,
    UserSelectorComponent,
    UserCardComponent
  ]
})
export class UserModule {
}
