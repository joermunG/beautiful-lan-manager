import {Location} from '@angular/common';
import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, TemplateRef} from '@angular/core';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {BehaviorSubject, combineLatest, Observable, Subject} from 'rxjs';
import {filter, map, takeUntil, tap} from 'rxjs/operators';
import {isNull} from 'util';
import {User} from '../../../classes/user/user';
import {CurrentUserApiService} from '../api/current-user-api.service';
import {DisplayImageComponent} from '../helper-classes/DisplayImageComponent';

@Component({
  selector: 'lan-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent extends DisplayImageComponent implements OnInit, OnChanges, OnDestroy {

  @Input() private user: User;
  @Input() disableMenu = false;
  @Input() size: 'tiny' | 'small' | 'medium' | 'large' | 'giant' = 'medium';

  constructor(private currentUserApi: CurrentUserApiService, location: Location, private dialog: NbDialogService) {
    super(location);
  }

  public modalRef: NbDialogRef<any>;
  private unsubscribe = new Subject();

  private imageChangeEvent: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  public image$ = this.imageChangeEvent.asObservable().pipe(
    filter(img => !isNull(img)),
    map(img => {
      return this.prepareImgUrl(img);
    })
  );

  private userChangeEvent: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  public user$: Observable<User> =
    combineLatest([this.userChangeEvent.asObservable(), this.currentUserApi.currentUser$]).pipe(
      filter(([user, _]: [User, User]) => !isNull(user)),
      tap(([user, _]: [User, User]) => this.imageChangeEvent.next(user.imageUrl)),
      map(([user, _]) => user),
    );

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('user') && changes.user.currentValue) {
      this.userChangeEvent.next(this.user);
    }
  }

  public openUserModal(modal: TemplateRef<any>) {
    this.modalRef = this.dialog.open(modal);
    this.modalRef.onClose.pipe(
      takeUntil(this.unsubscribe)
    ).subscribe(_ => this.modalRef = undefined);
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
  }
}
