import {Location} from '@angular/common';

export class DisplayImageComponent {

  constructor(private location: Location) {
  }

  public prepareImgUrl(slug: string): string {
    if (slug) {
      return this.location.prepareExternalUrl(slug);
    }
    return null;
  }
}
