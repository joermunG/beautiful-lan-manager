import {AbstractFilter} from '../../../filter/abstract-filter';
import {RelationalOperator} from '../../../filter/relational-operator';

export class UserFilter extends AbstractFilter {
  public id: Map<RelationalOperator, number> = new Map<RelationalOperator, number>();
}
