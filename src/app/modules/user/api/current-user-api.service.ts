import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {JsonConvert, OperationMode, ValueCheckingMode} from 'json2typescript';
import {BehaviorSubject, combineLatest, Observable, of} from 'rxjs';
import {catchError, filter, map, mapTo, shareReplay, switchMap, take, tap} from 'rxjs/operators';
import {isNull} from 'util';
import {User} from '../../../classes/user/user';
import {UserRole} from '../../../enums/user-role';
import {UsersApiService} from './users-api.service';

@Injectable({
  providedIn: 'root'
})
export class CurrentUserApiService {
  private jsonConvert: JsonConvert;

  constructor(private http: HttpClient, private usersApi: UsersApiService) {
    // this.reloadCurrentUser$.pipe(take(1)).subscribe();
    this.usersApi.someUserChanged$
      .pipe(
        switchMap(() => this.reloadCurrentUser$.pipe(take(1)))
      )
      .subscribe();

    this.jsonConvert = new JsonConvert();
    this.jsonConvert.operationMode = OperationMode.ENABLE; // print some debug data
    this.jsonConvert.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
    this.jsonConvert.valueCheckingMode = ValueCheckingMode.ALLOW_NULL; // always allow null
  }

  private currentUserCache: BehaviorSubject<User> = new BehaviorSubject<User>(null);

  public currentUser$: Observable<User> = this.currentUserCache.asObservable().pipe(
    switchMap(user => user ? of(user) : this.reloadCurrentUser$),
    filter(user => !isNull(user))
  );

  private reloadCurrentUser$ = this.http.get<User>('api/v1/Auth/user').pipe(
    map(resp => this.jsonConvert.deserializeObject(resp, User)),
    tap(user => {
      this.currentUserCache.next(user);
    }),
    shareReplay()
  );

  public currentUserHasRole$(role: UserRole): Observable<boolean> {
    return this.currentUser$.pipe(
      take(1),
      map(u => {
        return u.hasRole(role);
      })
    );
  }

  public isCurrentUser$(userId: User['id']): Observable<boolean> {
    return combineLatest([this.usersApi.getUserById(userId), this.currentUser$]).pipe(
      map((users: [User, User]) => {
        return users[0] && users[0].id === users[1].id;
      })
    );
  }

  public isAuthorized$(): Observable<boolean> {
    return this.reloadCurrentUser$.pipe(
      mapTo(true),
      catchError((_: HttpErrorResponse) => {
        this.currentUserCache.next(null);
        return of(false);
      }));
  }
}
