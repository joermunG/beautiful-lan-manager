import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {NbToastrService} from '@nebular/theme';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {catchError, map, take, tap} from 'rxjs/operators';
import {AbstractApiService} from '../../../abstract-services/abstract-api.service';
import {PagedResult} from '../../../classes/paged-result';
import {UpdateUser} from '../../../classes/user/update-user';
import {User} from '../../../classes/user/user';
import {Filter} from '../../../filter/filter';
import {RelationalOperator} from '../../../filter/relational-operator';
import {UserFilter} from './user-filter';

@Injectable({
  providedIn: 'root'
})
export class UsersApiService extends AbstractApiService<User> {

  baseUrl = '/api/v1/Users';

  private allUsers: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);
  /**
   * @deprecated Should be removed in favor of NotificationService
   */
  private someUserChanged: Subject<null> = new Subject<null>();

  public allUsers$: Observable<User[]> = this.allUsers.asObservable();
  /**
   * @deprecated Should be removed in favor of NotificationService
   */
  public someUserChanged$: Subject<null> = new Subject<null>();

  constructor(public http: HttpClient, private toast: NbToastrService) {
    super(User);
  }

  private reloadUsers() {
    const filter = new Filter(UserFilter);
    // Todo: use getALLfilteredusers
    this.getFilteredUsers(filter).subscribe(value => this.allUsers.next(value.results));
  }

  public updatePassword(userId: number, newPassword: string): Observable<User> {
    return this.putGeneric(this.baseUrl + '/' + userId + '/password', {newPassword}, User).pipe(
      catchError(() => {
        this.toast.danger('Updating password failed!');
        return of(null);
      })
    );
  }

  public updateUser(userId: number, user: UpdateUser): Observable<User> {
    return this.putGeneric(this.baseUrl + '/' + userId, user, User).pipe(
      tap((userResponse: User) => {
        this.someUserChanged.next();
      }),
      catchError(() => {
        this.toast.danger(user.nickname, 'Update Failed!');
        return of(null);
      })
    );
  }

  public getFilteredUsers(filter: Filter<UserFilter>): Observable<PagedResult<User>> {
    return this.getFiltered(this.baseUrl, filter);
  }

  public getUserById(id: number): Observable<User> {
    const filter = new Filter(UserFilter, 1, 1);
    filter.setFilter('id', RelationalOperator.EQUALS, id);
    return this.getFiltered(this.baseUrl, filter).pipe(
      take(1),
      map(pagedResult => pagedResult.results.shift())
    );
  }
}
