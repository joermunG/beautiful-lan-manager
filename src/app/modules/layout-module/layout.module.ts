import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {NbActionsModule, NbButtonModule, NbIconModule, NbLayoutModule, NbListModule, NbSidebarModule, NbUserModule} from '@nebular/theme';
import {ImageCropperModule} from 'ngx-image-cropper';
import {ImageCropperComponent} from './image-cropper/image-cropper.component';
import {PageForbiddenComponent} from './page-forbidden/page-forbidden.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';


@NgModule({
  declarations: [
    PageForbiddenComponent,
    PageNotFoundComponent,
    ImageCropperComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NbLayoutModule,
    NbIconModule,
    NbUserModule,
    NbButtonModule,
    ImageCropperModule,
    NbActionsModule,
    RouterModule,
    NbListModule,
    NbSidebarModule
  ], exports: [
    PageForbiddenComponent,
    PageNotFoundComponent,
    ImageCropperComponent
  ]
})
export class LayoutModule {
}
