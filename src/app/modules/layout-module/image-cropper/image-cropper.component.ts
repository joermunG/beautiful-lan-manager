import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'lan-image-cropper',
  templateUrl: './image-cropper.component.html',
  styleUrls: ['./image-cropper.component.scss']
})
export class ImageCropperComponent implements OnInit {

  @Input() aspectRatio = 1;
  @Input() resizeToWidth = 128;
  @Input() resizeToHeight = 128;
  @Input() showPreview = true;

  @Output() croppedImageEvent: EventEmitter<string> = new EventEmitter<string>();

  constructor() {
  }

  // image cropper events
  public onImageUploadEvent: EventEmitter<any> = new EventEmitter<any>();

  ngOnInit() {
  }
}
