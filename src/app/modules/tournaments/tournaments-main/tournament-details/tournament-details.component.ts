import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NbToastrService} from '@nebular/theme';
import {BehaviorSubject, combineLatest, from, Observable} from 'rxjs';
import {filter, map, switchMap, take, tap, toArray, withLatestFrom} from 'rxjs/operators';
import {TournamentState} from 'src/app/enums/tournament-state';
import {isNull, isUndefined} from 'util';
import {Tournament} from '../../../../classes/tournament/tournament';
import {TournamentMatch} from '../../../../classes/tournament/tournament-match';
import {TournamentRound} from '../../../../classes/tournament/tournament-round';
import {TournamentScore} from '../../../../classes/tournament/tournament-score';
import {TournamentTeam} from '../../../../classes/tournament-teams/tournament-team';
import {User} from '../../../../classes/user/user';
import {CurrentUserApiService} from '../../../user/api/current-user-api.service';
import {TournamentsApiService} from '../../api/tournaments-api.service';

@Component({
  selector: 'lan-tournament-details',
  templateUrl: './tournament-details.component.html',
  styleUrls: ['./tournament-details.component.scss']
})
export class TournamentDetailsComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private toast: NbToastrService,
              private tournamentsApi: TournamentsApiService,
              private currentUserApi: CurrentUserApiService) {
  }

  // Data
  public currentUser$: Observable<User> = this.currentUserApi.currentUser$;
  public tournament$: Observable<Tournament>;
  private tournamentEvent: BehaviorSubject<Tournament> = new BehaviorSubject<Tournament>(null);

  public currentUserMatches$: Observable<TournamentMatch[]>;
  public nextMatch$: Observable<TournamentMatch>;

  public highlightedTeam: TournamentTeam;


  // enums
  public TournamentState = TournamentState;

  ngOnInit() {
    this.tournament$ = this.tournamentEvent.asObservable().pipe(
      filter(t => !isNull(t))
    );

    this.route.data.pipe(
      take(1),
      map(data => data.hasOwnProperty('tournament') ? data.tournament : null),
      tap(tournament => this.tournamentEvent.next(tournament))
    ).subscribe();

    this.currentUserMatches$ = combineLatest([this.currentUser$, this.tournament$]).pipe(
      map(([_, tournament]) => tournament),
      switchMap(tournament => from(tournament.rounds).pipe(
        take(tournament.rounds.length),
        switchMap((rounds: TournamentRound) => from(rounds.matches)),
        switchMap((match: TournamentMatch) => from(match.scores).pipe( // iterate over all scores
          filter((score) => !isNull(score.tournamentTeam)), // check if a team exists
          withLatestFrom(this.currentUser$),
          filter(([score, currentUser]) =>
            !isNull(this.isCurrentUserInScore(score, currentUser))), // check if currentUser is in the team
          map((_) => match), // return match if currentUser is in team
        )),
        toArray(),
      ))
    );

    this.nextMatch$ = this.currentUserMatches$.pipe(
      switchMap(matches => from(matches)),
      filter(match => !match.isCompleted)
    );
  }

  public onReloadTournamentChange() {
    this.tournament$.pipe(
      take(1),
      switchMap(t => this.tournamentsApi.getTournamentById(t.id)),
      tap(t => this.tournamentEvent.next(t))
    ).subscribe();
  }

  public onTeamHover(team: TournamentTeam) {
    this.highlightedTeam = team;
  }

  private isCurrentUserInScore(score: TournamentScore, currentUser: User): TournamentScore | null {
    return !isUndefined(score.tournamentTeam.members.find((m) => m.member.id === currentUser.id)) ? score : null;
  }
}
