import {Component, EventEmitter, Input, OnInit, Output, TemplateRef} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {of, throwError} from 'rxjs';
import {catchError, filter, switchMap, take, tap} from 'rxjs/operators';
import {isUndefined} from 'util';
import {Tournament} from '../../../../../classes/tournament/tournament';
import {TournamentMatch} from '../../../../../classes/tournament/tournament-match';
import {TournamentScore} from '../../../../../classes/tournament/tournament-score';
import {TournamentTeam} from '../../../../../classes/tournament-teams/tournament-team';
import {UpdateMatchScore} from '../../../../../classes/tournament/update-match-score';
import {User} from '../../../../../classes/user/user';
import {MatchScoreValidator} from '../../../../../validators/match-score-validator';
import {CurrentUserApiService} from '../../../../user/api/current-user-api.service';
import {TournamentsApiService} from '../../../api/tournaments-api.service';

@Component({
  selector: 'lan-tournament-next-match-card',
  templateUrl: './tournament-next-match-card.component.html',
  styleUrls: ['./tournament-next-match-card.component.scss']
})
export class TournamentNextMatchCardComponent implements OnInit {

  @Input() match: TournamentMatch;
  @Input() tournament: Tournament;
  @Output() teamHover: EventEmitter<TournamentTeam> = new EventEmitter<TournamentTeam>();
  @Output() reloadTournamentChange: EventEmitter<any> = new EventEmitter<any>();

  constructor(private toast: NbToastrService,
              private currentUserApi: CurrentUserApiService,
              private tournamentsApi: TournamentsApiService,
              private dialogService: NbDialogService) {
  }

  public scoreForm = new FormGroup({
    scores: new FormArray([], MatchScoreValidator)
  });

  public modalRef: NbDialogRef<any>;

  private currentUser: User;

  ngOnInit() {
    this.currentUserApi.currentUser$.pipe(
      take(1),
      tap(u => this.currentUser = u)
    ).subscribe();
  }

  openScoreModal(template: TemplateRef<any>, match: TournamentMatch) {
    match.scores.forEach(_ => {
      this.scoresFormList.push(new FormControl(0,
        Validators.compose([Validators.required, Validators.min(0)])));
    });
    this.modalRef = this.dialogService.open(template, {context: match});
    this.modalRef.onClose.pipe(take(1)).subscribe(() => {
      this.resetScoreForm();
    });
  }

  saveScore(match: TournamentMatch) {
    const setScoreMessage = new UpdateMatchScore();
    let currentUserScore = 0;
    let highestScore = 0;

    match.scores.forEach((s: TournamentScore, index) => {
      const scoreInForm = this.scoresFormList.controls[index].value;
      setScoreMessage.scores.push({tournamentTeamId: s.tournamentTeam.id, score: scoreInForm});

      if (this.isCurrentUserInScore(s, this.currentUser)) {
        currentUserScore = scoreInForm;
      }

      highestScore = scoreInForm > highestScore ? scoreInForm : highestScore;
    });

    of(setScoreMessage).pipe(
      take(1),
      filter(_ => this.scoreForm.valid),
      switchMap(scoreMsg => currentUserScore < highestScore ?
        of(scoreMsg) :
        throwError('The Score has to be entered by the Loser!')),

      switchMap(scoreMsg => this.tournamentsApi.setMatchScore(match, scoreMsg)),
      catchError(errMsg => {
        this.toast.danger(errMsg, 'Failed to set the score', {destroyByClick: true, duration: 0});
        return of(false);
      }),
      filter(isSuccess => isSuccess),
      tap(_ => this.toast.success('Score set.', 'Success', {destroyByClick: true, duration: 2000})),
      tap(_ => this.reloadTournamentChange.emit()),
      tap(_ => this.modalRef.close())
    ).subscribe();
  }

  private isCurrentUserInScore(score: TournamentScore, currentUser: User): TournamentScore | null {
    return !isUndefined(score.tournamentTeam.members.find((m) => m.member.id === currentUser.id)) ? score : null;
  }

  private resetScoreForm() {
    this.scoresFormList.clear();
    this.scoreForm.reset();
  }

  get scoresFormList(): FormArray {
    return this.scoreForm.get('scores') as FormArray;
  }


}
