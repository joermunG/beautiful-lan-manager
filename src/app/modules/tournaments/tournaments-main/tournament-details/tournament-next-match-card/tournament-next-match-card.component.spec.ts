import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentNextMatchCardComponent } from './tournament-next-match-card.component';

describe('TournamentNextMatchCardComponent', () => {
  let component: TournamentNextMatchCardComponent;
  let fixture: ComponentFixture<TournamentNextMatchCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TournamentNextMatchCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentNextMatchCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
