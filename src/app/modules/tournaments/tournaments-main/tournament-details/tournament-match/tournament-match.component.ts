import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faTrophy} from '@fortawesome/free-solid-svg-icons/faTrophy';
import {Tournament} from '../../../../../classes/tournament/tournament';
import {TournamentMatch} from '../../../../../classes/tournament/tournament-match';
import {TournamentTeam} from '../../../../../classes/tournament-teams/tournament-team';

@Component({
  selector: 'lan-tournament-match',
  templateUrl: './tournament-match.component.html',
  styleUrls: ['./tournament-match.component.scss']
})
export class TournamentMatchComponent implements OnInit {

  @Input() match: TournamentMatch;
  @Input() highlightedTeam: TournamentTeam;
  @Input() tournament: Tournament;
  @Output() teamHover: EventEmitter<TournamentTeam> = new EventEmitter<TournamentTeam>();

  constructor() {
  }

  public faTrophy = faTrophy;

  ngOnInit() {
  }

  public get teamIsHighlighted(): boolean {
    if (!this.highlightedTeam) {
      return false;
    }
    if (!this.match) {
      return false;
    }

    let isHighlighted = false;
    this.match.scores.forEach(s => {
      if (s.tournamentTeam && s.tournamentTeam.id && !isHighlighted) {
        isHighlighted = s.tournamentTeam.id === this.highlightedTeam.id;
      }
    });
    return isHighlighted;
  }
}
