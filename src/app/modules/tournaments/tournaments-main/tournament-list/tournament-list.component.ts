import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Router} from '@angular/router';
import {PagedResult} from '../../../../classes/paged-result';
import {Tournament} from '../../../../classes/tournament/tournament';
import {TournamentState} from '../../../../enums/tournament-state';
import {CurrentUserApiService} from '../../../user/api/current-user-api.service';

@Component({
  selector: 'lan-tournament-list',
  templateUrl: './tournament-list.component.html',
  styleUrls: ['./tournament-list.component.scss']
})
export class TournamentListComponent {

  @Input() tournaments: PagedResult<Tournament> | null;
  @Output() tournamentUpdate: EventEmitter<Tournament> = new EventEmitter<Tournament>();
  @Output() tournamentDelete: EventEmitter<Tournament> = new EventEmitter<Tournament>();
  @Output() tournamentStart: EventEmitter<Tournament> = new EventEmitter<Tournament>();
  @Output() tournamentOpenRegistration: EventEmitter<Tournament> = new EventEmitter<Tournament>();

  constructor(private router: Router,
              private currentUserApi: CurrentUserApiService) {
  }

  public TournamentState = TournamentState;
  public currentUser$ = this.currentUserApi.currentUser$;
}
