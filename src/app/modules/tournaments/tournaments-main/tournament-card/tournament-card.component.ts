import {Location} from '@angular/common';
import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {take, tap} from 'rxjs/operators';
import {TournamentState} from 'src/app/enums/tournament-state';
import {UserRole} from 'src/app/enums/user-role';
import {Tournament} from '../../../../classes/tournament/tournament';
import {TournamentTeam} from '../../../../classes/tournament-teams/tournament-team';
import {User} from '../../../../classes/user/user';
import {CurrentUserApiService} from '../../../user/api/current-user-api.service';
import {TournamentTeamApiService} from '../../api/tournament-team-api.service';

@Component({
  selector: 'lan-tournament-card',
  templateUrl: './tournament-card.component.html',
  styleUrls: ['./tournament-card.component.scss']
})
export class TournamentCardComponent implements OnInit, OnChanges {

  @Input() tournament: Tournament;
  @Output() tournamentUpdate: EventEmitter<Tournament> = new EventEmitter<Tournament>();
  @Output() tournamentDelete: EventEmitter<Tournament> = new EventEmitter<Tournament>();
  @Output() tournamentStart: EventEmitter<Tournament> = new EventEmitter<Tournament>();
  @Output() tournamentOpenRegistration: EventEmitter<Tournament> = new EventEmitter<Tournament>();


  constructor(private location: Location,
              private currentUserApi: CurrentUserApiService,
              private teamsApi: TournamentTeamApiService,
              private router: Router) {
  }

  public image: string;
  public TournamentState = TournamentState;

  public currentUser$ = this.currentUserApi.currentUser$;
  public userIsManager$: Observable<boolean> = this.currentUserApi.currentUserHasRole$(UserRole.MANAGER);

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('tournament') && changes.tournament.currentValue) {
      this.image = this.prepareImgUrl(this.tournament.imageUrl);
    }
  }


  public register(tournament: Tournament) {
    this.router.navigate(['tournaments', tournament.id, 'register']);
  }

  public unregister(team: TournamentTeam, user: User) {
    this.teamsApi.leave(team, user).pipe(
      take(1),
      tap(() => {
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate(['tournaments']);
      })
    ).subscribe();
  }

  private prepareImgUrl(slug: string): string | null {
    if (slug) {
      return this.location.prepareExternalUrl(slug);
    }
    return null;
  }

  // used for codehint in templates
  public asTournament(t: Tournament): Tournament {
    return t as Tournament;
  }
}
