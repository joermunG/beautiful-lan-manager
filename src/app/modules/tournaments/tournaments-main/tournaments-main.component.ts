import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {catchError, filter, map, switchMap, take, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {isNull} from 'util';
import {PagedResult} from '../../../classes/paged-result';
import {Tournament} from '../../../classes/tournament/tournament';
import {UpdateTournament} from '../../../classes/tournament/update-tournament';
import {TournamentState} from '../../../enums/tournament-state';
import {UserRole} from '../../../enums/user-role';
import {Filter} from '../../../filter/filter';
import {RelationalOperator} from '../../../filter/relational-operator';
import {EventsApiService} from '../../event/api/events-api.service';
import {CurrentUserApiService} from '../../user/api/current-user-api.service';
import {TournamentFilter} from '../api/tournament-filter';
import {TournamentsApiService} from '../api/tournaments-api.service';

@Component({
  selector: 'lan-tournaments-main',
  templateUrl: './tournaments-main.component.html',
  styleUrls: ['./tournaments-main.component.scss']
})
export class TournamentsMainComponent implements OnInit, OnDestroy {

  constructor(private currentUserApiService: CurrentUserApiService,
              private route: ActivatedRoute,
              private router: Router,
              private tournamentsApi: TournamentsApiService,
              private eventsApi: EventsApiService,
              private dialogService: NbDialogService,
              private toast: NbToastrService) {
  }

  public userIsManager$ = this.currentUserApiService.currentUserHasRole$(UserRole.MANAGER);

  // Tournament Data ------------------
  private plannedTournamentsChange: BehaviorSubject<PagedResult<Tournament>> = new BehaviorSubject<PagedResult<Tournament>>(null);
  private activeTournamentsChange: BehaviorSubject<PagedResult<Tournament>> = new BehaviorSubject<PagedResult<Tournament>>(null);
  private finishedTournamentsChange: BehaviorSubject<PagedResult<Tournament>> = new BehaviorSubject<PagedResult<Tournament>>(null);

  public plannedTournaments$: Observable<PagedResult<Tournament>> = this.plannedTournamentsChange.asObservable();
  public activeTournaments$: Observable<PagedResult<Tournament>> = this.activeTournamentsChange.asObservable();
  public finishedTournaments$: Observable<PagedResult<Tournament>> = this.finishedTournamentsChange.asObservable();

  public startTournamentEvent: Subject<Tournament> = new Subject<Tournament>();
  public saveTournamentUpdateEvent: Subject<[Tournament['id'], UpdateTournament]> = new Subject<[Tournament['id'], UpdateTournament]>();
  public deleteTournamentEvent: Subject<Tournament> = new Subject<Tournament>();
  // ----------------------------------

  public modalRef: NbDialogRef<any>;
  private unsubscribe: Subject<any> = new Subject<any>();

  ngOnInit() {
    this.eventsApi.currentEvent$.pipe(
      // Todo: ask Olli if we need this "asObservable()" here
      takeUntil(this.unsubscribe.asObservable())
    ).subscribe(event => {
      this.reloadTournamentLists(event.id);
    });

    // subscribe to start events
    this.startTournamentEvent.pipe(
      takeUntil(this.unsubscribe.asObservable()),
      switchMap(tournament => {
        return this.tournamentsApi.startTournament(tournament.id).pipe(withLatestFrom(of(tournament)));
      }),
      tap(([isSuccess, tournament]) => {
        if (isSuccess) {
          this.toast.success(tournament.name + 'started!', 'Success!', {destroyByClick: true});
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate(['/tournaments', tournament.id]);
        }
      }),
      catchError(() => {
        return of(null);
      }),
      filter(resp => !isNull(resp)),
      map(resp => resp[0])
    ).subscribe();

    // subscribe to update events
    this.saveTournamentUpdateEvent.pipe(
      takeUntil(this.unsubscribe.asObservable()),
      switchMap((payload: [Tournament['id'], UpdateTournament]) => this.tournamentsApi.updateTournament(payload[0], payload[1])),
      tap((updatedTournament: Tournament) => {
        this.toast.success(updatedTournament.name + ' updated', 'Success!', {destroyByClick: true});
        if (this.modalRef) {
          this.modalRef.close();
        }
        this.eventsApi.currentEvent$.pipe(
          take(1)
        ).subscribe(event => {
          this.reloadTournamentLists(event.id);
        });
      })
    ).subscribe();

    // subscribe to update events
    this.deleteTournamentEvent.pipe(
      takeUntil(this.unsubscribe.asObservable()),
      switchMap(tournament => this.tournamentsApi.deleteTournament(tournament.id).pipe(map(() => tournament))),
      tap((tournament: Tournament) => {
        this.toast.success('Tournament "' + tournament.name + '" deleted', 'Success!', {destroyByClick: true});
        this.eventsApi.currentEvent$.pipe(
          take(1)
        ).subscribe(event => {
          this.reloadTournamentLists(event.id);
        });
      }),
    ).subscribe();
  }

  public startRegistration(tournament: Tournament) {
    const updateTournament = new UpdateTournament();
    updateTournament.name = tournament.name;
    updateTournament.playerPerTeam = tournament.playerPerTeam;
    updateTournament.mode = tournament.mode;
    updateTournament.state = TournamentState.OPEN_FOR_REGISTRATION;
    console.log(updateTournament);
    this.saveTournamentUpdateEvent.next([tournament.id, updateTournament]);
  }

  public openUpdateModal(t: Tournament, updateDialog: TemplateRef<any>) {
    this.modalRef = this.dialogService.open(updateDialog, {context: t});
    this.modalRef.onClose.pipe(take(1)).subscribe(_ => this.modalRef = undefined);
  }

  private reloadTournamentLists(eventId: number) {
    const tournamentFilter = new Filter(TournamentFilter);
    tournamentFilter.setFilter('eventId', RelationalOperator.EQUALS, eventId);
    tournamentFilter.setSort('state', 'ASC');
    tournamentFilter.setSort('name', 'ASC');

    tournamentFilter.setFilter('state', RelationalOperator.EQUALS, [TournamentState.RUNNING, TournamentState.OPEN_FOR_REGISTRATION]);
    this.tournamentsApi.getFilteredTournaments(tournamentFilter).subscribe(
      pr => this.activeTournamentsChange.next(pr)
    );

    tournamentFilter.setFilter('state', RelationalOperator.EQUALS, [TournamentState.FINISHED]);
    this.tournamentsApi.getFilteredTournaments(tournamentFilter).subscribe(
      pr => this.finishedTournamentsChange.next(pr)
    );

    tournamentFilter.setFilter('state', RelationalOperator.EQUALS, TournamentState.PLANNING);
    this.currentUserApiService.currentUserHasRole$(UserRole.MANAGER).pipe(
      take(1),
      switchMap(isManager => {
        return isManager ? this.tournamentsApi.getFilteredTournaments(tournamentFilter) : of(null);
      })
    ).subscribe(
      pr => this.plannedTournamentsChange.next(pr)
    );
  }

  public async addNewTournament() {
    await this.router.navigate(['new'], {relativeTo: this.route});
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
