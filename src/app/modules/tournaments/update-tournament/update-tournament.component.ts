import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NbComponentStatus} from '@nebular/theme';
import {Subject} from 'rxjs';
import {takeUntil, tap} from 'rxjs/operators';
import {Tournament} from '../../../classes/tournament/tournament';
import {UpdateTournament} from '../../../classes/tournament/update-tournament';

@Component({
  selector: 'lan-update-tournament',
  templateUrl: './update-tournament.component.html',
  styleUrls: ['./update-tournament.component.scss']
})
export class UpdateTournamentComponent implements OnInit, OnChanges, OnDestroy {

  @Input() tournament: Tournament;
  @Output() save: EventEmitter<[Tournament['id'], UpdateTournament]> = new EventEmitter<[Tournament['id'], UpdateTournament]>();
  @Output() cancel: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
  }

  public form = new FormGroup({
    name: new FormControl(null,
      {
        validators: Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(100)]),
        updateOn: 'blur'
      }
    ),
    playerPerTeam: new FormControl(null,
      {
        validators: Validators.compose([Validators.required, Validators.min(1)]),
        updateOn: 'blur',
      }
    ),
    mode: new FormControl(null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(100)])),
    image: new FormControl(null, Validators.compose([Validators.required])),
    eventId: new FormControl(null)
  });

  public fileConfirmationEvent: EventEmitter<string> = new EventEmitter<string>();

  private unsubscribe: Subject<any> = new Subject<any>();

  ngOnInit() {
    this.fileConfirmationEvent.pipe(
      takeUntil(this.unsubscribe.asObservable()),
      tap(img => {
        this.form.get('image').patchValue(img);
        this.form.get('image').markAsTouched();
      })
    ).subscribe();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('tournament') && changes.tournament.currentValue) {
      const t: Tournament = changes.tournament.currentValue;
      this.form.patchValue({
        name: t.name,
        playerPerTeam: t.playerPerTeam,
        mode: t.mode,
        eventId: t.event.id,
      });
    }
  }

  public submit() {
    if (this.form.valid) {
      const payload: UpdateTournament = Object.assign(new UpdateTournament(), this.form.getRawValue());
      // payload.state = this.tournament.state;
      this.save.emit([this.tournament.id, payload]);
    }
  }

  public formFieldStatus(fieldName: string): NbComponentStatus {
    const field = this.form.get(fieldName);
    if (!field.touched) {
      return null;
    }
    if (field.touched && field.invalid) {
      return 'danger';
    }
    if (field.touched && field.valid) {
      return 'primary';
    }
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
