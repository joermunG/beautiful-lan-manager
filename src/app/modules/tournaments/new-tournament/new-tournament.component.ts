import {Component, EventEmitter, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {NbComponentStatus, NbToastrService} from '@nebular/theme';
import {Subject} from 'rxjs';
import {take, takeUntil, tap} from 'rxjs/operators';
import {CreateTournament} from '../../../classes/tournament/create-tournament';
import {EventsApiService} from '../../event/api/events-api.service';
import {TournamentsApiService} from '../api/tournaments-api.service';

@Component({
  selector: 'lan-new-tournament',
  templateUrl: './new-tournament.component.html',
  styleUrls: ['./new-tournament.component.scss']
})
export class NewTournamentComponent implements OnInit, OnDestroy {

  constructor(private tournamentsApi: TournamentsApiService,
              private eventsApi: EventsApiService,
              private toast: NbToastrService,
              private router: Router) {
  }

  public form = new FormGroup({
    name: new FormControl(null,
      {
        validators: Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(100)]),
        updateOn: 'blur'
      }
    ),
    playerPerTeam: new FormControl(null,
      {
        validators: Validators.compose([Validators.required, Validators.min(1)]),
        updateOn: 'blur'
      }
    ),
    mode: new FormControl(null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(100)])),
    image: new FormControl(null, Validators.compose([Validators.required])),
    eventId: new FormControl(null)
  });

  public fileConfirmationEvent: EventEmitter<string> = new EventEmitter<string>();

  private unsubscribe: Subject<any> = new Subject<any>();

  ngOnInit() {
    this.eventsApi.currentEvent$.pipe(
      take(1),
      tap(event => {
        const formData = new CreateTournament();
        formData.eventId = event.id;
        this.form.patchValue({
          name: formData.name,
          playerPerTeam: formData.playerPerTeam,
          mode: formData.mode,
          eventId: formData.eventId,
          image: formData.image
        });
      })
    ).subscribe();

    this.fileConfirmationEvent.pipe(
      takeUntil(this.unsubscribe.asObservable()),
      tap(img => {
        this.form.get('image').patchValue(img);
        this.form.get('image').markAsTouched();
      })
    ).subscribe();
  }

  public submit() {
    if (this.form.valid) {
      this.tournamentsApi.createTournament(this.form.getRawValue()).pipe(
        take(1),
        tap(resp => {
          this.toast.success(resp.name, 'New Tournament Created', {
            destroyByClick: true,
            duration: 10000,
            hasIcon: true,
            icon: 'checkmark-circle-outline'
          });
          this.router.navigate(['tournaments']);
        })
      ).subscribe();
    }
  }

  public formFieldStatus(fieldName: string): NbComponentStatus {
    const field = this.form.get(fieldName);
    if (!field.touched) {
      return null;
    }
    if (field.touched && field.invalid) {
      return 'danger';
    }
    if (field.touched && field.valid) {
      return 'primary';
    }
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
