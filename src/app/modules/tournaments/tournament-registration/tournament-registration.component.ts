import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NbComponentStatus} from '@nebular/theme/components/component-status';
import {BehaviorSubject, NEVER, Observable} from 'rxjs';
import {catchError, delay, map, switchMap, take, tap} from 'rxjs/operators';
import {CreateTournamentTeam} from '../../../classes/tournament-teams/create-tournament-team';
import {Tournament} from '../../../classes/tournament/tournament';
import {CurrentUserApiService} from '../../user/api/current-user-api.service';
import {TournamentTeamApiService} from '../api/tournament-team-api.service';

@Component({
  selector: 'lan-tournament-registration',
  templateUrl: './tournament-registration.component.html',
  styleUrls: ['./tournament-registration.component.scss']
})
export class TournamentRegistrationComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private router: Router,
              private tournamentTeamApi: TournamentTeamApiService,
              private currentUserApi: CurrentUserApiService) {
  }

  public accent: NbComponentStatus = 'info';

  public tournament$: Observable<Tournament>;
  private isLoadingEvent: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  public isLoading$ = this.isLoadingEvent.asObservable();

  private errorEvent: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  public error$ = this.errorEvent.asObservable();

  ngOnInit() {
    this.tournament$ = this.route.data.pipe(map(data => {
        return data.hasOwnProperty('tournament') ? data.tournament as Tournament : null;
      }), tap(tournament => {
        if (tournament && tournament.playerPerTeam === 1) {
          this.automaticRegistrationFor1v1(tournament);
        }
      })
    );
  }

  public createNewTournamentTeam(newTeam: CreateTournamentTeam) {
    this.tournamentTeamApi.createTeam(newTeam).pipe(
      take(1),
      tap(_ => this.router.navigate(['/tournaments', newTeam.tournamentId, 'teams']))
    ).subscribe();
  }

  private automaticRegistrationFor1v1(tournament: Tournament) {
    if (tournament.isLocked) {
      this.errorEvent.next('Tournament already Started.');
      this.accent = 'danger';
      this.isLoadingEvent.next(false);
      setTimeout(() => this.router.navigate(['/tournaments']),
        2000);
      return;
    }
    this.currentUserApi.currentUser$.pipe(
      take(1),
      tap(() => this.isLoadingEvent.next(true)),
      // create a team
      switchMap(user => {
        const newTeam = new CreateTournamentTeam();
        newTeam.tournamentId = tournament.id;
        newTeam.name = user.nickname + user.id;
        return this.tournamentTeamApi.createTeam(newTeam).pipe(catchError(error => {
            if (error.error.errors.hasOwnProperty('Name')) {
              this.errorEvent.next('You already joined.');
              this.accent = 'danger';
              this.isLoadingEvent.next(false);
            }
            return NEVER;
          })
        );
      }),
      // bit of animation and navigate back to room overview
      delay(2000),
      tap(() => {
        this.accent = 'success';
        this.isLoadingEvent.next(false);
      }),
      delay(2000),
      tap(() => {
        this.router.navigate(['/tournaments']);
      })
    ).subscribe();
  }
}
