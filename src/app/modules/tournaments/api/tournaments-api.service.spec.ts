import { TestBed } from '@angular/core/testing';

import { TournamentsApiService } from './tournaments-api.service';

describe('TournamentsApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TournamentsApiService = TestBed.get(TournamentsApiService);
    expect(service).toBeTruthy();
  });
});
