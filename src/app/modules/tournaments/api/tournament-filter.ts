import {AbstractFilter} from '../../../filter/abstract-filter';
import {RelationalOperator} from '../../../filter/relational-operator';
import {TournamentState} from '../../../enums/tournament-state';

export class TournamentFilter extends AbstractFilter {
  public id: Map<RelationalOperator, number | number[]> = new Map<RelationalOperator, number | number[]>();
  public name: Map<RelationalOperator, string | string[]> = new Map<RelationalOperator, string | string[]>();
  public state: Map<RelationalOperator, TournamentState | TournamentState[]> = new Map<RelationalOperator, TournamentState | TournamentState[]>();
  public eventId: Map<RelationalOperator, number | number[]> = new Map<RelationalOperator, number | number[]>();
}
