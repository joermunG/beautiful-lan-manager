import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {NbToastrService} from '@nebular/theme';
import {Observable, of} from 'rxjs';
import {catchError, filter, map, switchMap, tap} from 'rxjs/operators';
import {isNull} from 'util';
import {AbstractApiService} from '../../../abstract-services/abstract-api.service';
import {CreateTournamentTeam} from '../../../classes/tournament-teams/create-tournament-team';
import {TournamentTeam} from '../../../classes/tournament-teams/tournament-team';
import {User} from '../../../classes/user/user';
import {Filter} from '../../../filter/filter';
import {RelationalOperator} from '../../../filter/relational-operator';
import {TournamentTeamFilter} from './tournament-team-filter';

@Injectable({
  providedIn: 'root'
})
export class TournamentTeamApiService extends AbstractApiService<TournamentTeam> {
  baseUrl = '/api/v1/TournamentTeams';

  constructor(public http: HttpClient, private toast: NbToastrService) {
    super(TournamentTeam);
  }

  public getTournamentTeamById(teamId: number): Observable<TournamentTeam> {
    const tournamentTeamFilter = new Filter(TournamentTeamFilter, 1, 1);
    tournamentTeamFilter.setFilter('id', RelationalOperator.EQUALS, teamId);
    return this.getFiltered(this.baseUrl, tournamentTeamFilter).pipe(
      map(pagedResult => pagedResult.results ? pagedResult.results.shift() : null)
    );
  }

  public createTeam(team: CreateTournamentTeam): Observable<TournamentTeam> {
    return this.postGeneric(this.baseUrl, this.jsonConvert.deserializeObject(team, CreateTournamentTeam), TournamentTeam);
  }

  public invite(team: TournamentTeam, user: User): Observable<TournamentTeam> {
    return this.http.post<TournamentTeam>('/api/v1/TournamentTeams/' + team.id + '/invite', {userId: user.id}).pipe(
      map(resp => this.jsonConvert.deserializeObject(resp, TournamentTeam)),
      catchError(err => {
        this.toast.danger(err.error.errors.id[0], 'Error');
        return of(null);
      }),
      filter(resp => !isNull(resp))
    );
  }

  public revokeInvitation(team: TournamentTeam, user: User): Observable<TournamentTeam> {
    return this.http.post<TournamentTeam>('/api/v1/TournamentTeams/' + team.id + '/revoke-invitation', {userId: user.id}).pipe(
      map(resp => this.jsonConvert.deserializeObject(resp, TournamentTeam)),
      catchError(err => {
        this.toast.danger(err.error.errors.id[0], 'Error');
        return of(null);
      }),
      filter(resp => !isNull(resp))
    );
  }

  public join(team: TournamentTeam, user: User): Observable<TournamentTeam> {
    return this.http.post<TournamentTeam>('/api/v1/TournamentTeams/' + team.id + '/join', {userId: user.id}).pipe(
      map(resp => this.jsonConvert.deserializeObject(resp, TournamentTeam)),
      catchError(err => {
        this.toast.danger(err.error.errors.id[0], 'Error');
        return of(null);
      }),
      filter(resp => !isNull(resp))
    );
  }

  public leave(team: TournamentTeam, user: User): Observable<TournamentTeam> {
    return this.http.post('/api/v1/TournamentTeams/' + team.id + '/leave', {userId: user.id}).pipe(
      switchMap(_ => this.getTournamentTeamById(team.id)),
      tap(resp => console.log(resp), err => console.log(err)),
      catchError(err => {
        this.toast.danger(
          err && err.error && err.error.errors.hasOwnProperty('id') ?
            err.error.errors.id[0] :
            'Something bad happened', 'Error');
        return of(null);
      })
    );
  }

  public grantLeader(team: TournamentTeam, user: User): Observable<TournamentTeam> {
    return this.http.put<TournamentTeam>('/api/v1/TournamentTeams/' + team.id + '/grant-ownership/' + user.id, null).pipe(
      switchMap(_ => this.getTournamentTeamById(team.id)),
      catchError(err => {
        this.toast.danger(err.error.errors.id[0], 'Error');
        return of(null);
      }),
      filter(resp => !isNull(resp))
    );
  }

  public revokeLeader(team: TournamentTeam, user: User): Observable<TournamentTeam> {
    return this.http.put<TournamentTeam>('/api/v1/TournamentTeams/' + team.id + '/revoke-ownership/' + user.id, null).pipe(
      switchMap(_ => this.getTournamentTeamById(team.id)),
      catchError(err => {
        this.toast.danger(err.error.errors.id[0], 'Error');
        return of(null);
      }),
      filter(resp => !isNull(resp))
    );
  }

  public deleteTeam(team: TournamentTeam): Observable<null> {
    return this.deleteGeneric(this.baseUrl + '/' + team.id, null);
  }
}
