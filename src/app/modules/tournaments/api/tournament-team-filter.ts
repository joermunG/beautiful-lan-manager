import {AbstractFilter} from '../../../filter/abstract-filter';
import {RelationalOperator} from '../../../filter/relational-operator';

export class TournamentTeamFilter extends AbstractFilter {
  public id: Map<RelationalOperator, number | number[]> = new Map<RelationalOperator, number | number[]>();
  public name: Map<RelationalOperator, string | string[]> = new Map<RelationalOperator, string | string[]>();
  public eventId: Map<RelationalOperator, number | number[]> = new Map<RelationalOperator, number | number[]>();
}
