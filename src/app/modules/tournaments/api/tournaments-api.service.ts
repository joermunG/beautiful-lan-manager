import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {NbToastrService} from '@nebular/theme';
import {Observable, of, throwError} from 'rxjs';
import {catchError, map, mapTo} from 'rxjs/operators';
import {AbstractApiService} from '../../../abstract-services/abstract-api.service';
import {PagedResult} from '../../../classes/paged-result';
import {CreateTournament} from '../../../classes/tournament/create-tournament';
import {Tournament} from '../../../classes/tournament/tournament';
import {TournamentMatch} from '../../../classes/tournament/tournament-match';
import {UpdateMatchScore} from '../../../classes/tournament/update-match-score';
import {UpdateTournament} from '../../../classes/tournament/update-tournament';
import {Filter} from '../../../filter/filter';
import {RelationalOperator} from '../../../filter/relational-operator';
import {TournamentFilter} from './tournament-filter';

@Injectable({
  providedIn: 'root'
})
export class TournamentsApiService extends AbstractApiService<Tournament> {
  baseUrl = '/api/v1/Tournaments';

  constructor(public http: HttpClient, private toast: NbToastrService) {
    super(Tournament);
  }

  public getFilteredTournaments(filter: Filter<TournamentFilter>): Observable<PagedResult<Tournament>> {
    return this.getFiltered(this.baseUrl, filter);
  }

  public getTournamentById(tournamentId: number): Observable<Tournament> {
    const tournamentFilter = new Filter(TournamentFilter, 1, 1);
    tournamentFilter.setFilter('id', RelationalOperator.EQUALS, tournamentId);
    return this.getFiltered(this.baseUrl, tournamentFilter).pipe(
      map(t => {
        if (t.rowCount !== 1) {
          throwError(new Error('Tournament with id ' + tournamentId + ' not found'));
        } else {
          return t;
        }
      }),
      map(pagedResult => pagedResult.results.shift())
    );
  }

  public createTournament(createMessage: CreateTournament): Observable<Tournament> {
    return this.postGeneric(this.baseUrl, this.jsonConvert.deserializeObject(createMessage, CreateTournament), Tournament).pipe(
      catchError((err) => {
        this.toast.danger(createMessage.name + ' could not be created!', 'Error');
        return throwError(err);
      })
    );
  }

  public startTournament(tournamentId: Tournament['id']): Observable<boolean> {
    return this.putGeneric(
      this.baseUrl + '/' + tournamentId + '/start', null, Tournament).pipe(
      mapTo(true),
      catchError((err) => {
        this.toast.danger(err.error.errors.id[0], 'Start Failed!');
        return throwError(err);
      })
    );
  }

  public updateTournament(tournamentId: number, updateMessage: UpdateTournament): Observable<Tournament> {
    return this.putGeneric(
      this.baseUrl + '/' + tournamentId, updateMessage, Tournament).pipe(
      catchError((err) => {
        this.toast.danger(tournamentId, 'Update Failed!');
        return throwError(err);
      })
    );
  }

  public deleteTournament(tournamentId: number): Observable<null> {
    return this.deleteGeneric<null>(this.baseUrl + '/' + tournamentId, null).pipe(
      catchError((err) => {
        this.toast.danger(tournamentId, 'Deletion of Tournament Failed!');
        return throwError(err);
      })
    );
  }

  public setMatchScore(match: TournamentMatch, payload: UpdateMatchScore): Observable<boolean> {
    return this.putGeneric(this.baseUrl + '/score-match/' + match.id, payload, TournamentMatch).pipe(
      mapTo(true),
      catchError(_ => of(false))
    );
  }
}
