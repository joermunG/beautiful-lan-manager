import { TestBed } from '@angular/core/testing';

import { TournamentTeamApiService } from './tournament-team-api.service';

describe('TournamentTeamApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TournamentTeamApiService = TestBed.get(TournamentTeamApiService);
    expect(service).toBeTruthy();
  });
});
