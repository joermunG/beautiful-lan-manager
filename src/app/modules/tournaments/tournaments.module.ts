import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {
  NbButtonModule,
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbListModule, NbPopoverModule,
  NbRadioModule,
  NbSpinnerModule,
  NbTabsetModule,
  NbTooltipModule,
  NbUserModule
} from '@nebular/theme';
import {NgTournamentTreeModule} from 'ng-tournament-tree';
import {LayoutModule} from '../layout-module/layout.module';
import {TournamentTeamsModule} from '../tournament-teams/tournament-teams.module';
import {UserModule} from '../user/user.module';
import {NewTournamentComponent} from './new-tournament/new-tournament.component';
import {TournamentRegistrationComponent} from './tournament-registration/tournament-registration.component';
import {TournamentTeamComponent} from '../tournament-teams/tournament-team/tournament-team.component';
import {TournamentDetailsComponent} from './tournaments-main/tournament-details/tournament-details.component';
import {TournamentListComponent} from './tournaments-main/tournament-list/tournament-list.component';
import {TournamentsMainComponent} from './tournaments-main/tournaments-main.component';
import {TournamentMatchComponent} from './tournaments-main/tournament-details/tournament-match/tournament-match.component';
import {TournamentNextMatchCardComponent} from './tournaments-main/tournament-details/tournament-next-match-card/tournament-next-match-card.component';
import {TournamentCardComponent} from './tournaments-main/tournament-card/tournament-card.component';
import {UpdateTournamentComponent} from './update-tournament/update-tournament.component';


@NgModule({
  declarations: [
    TournamentsMainComponent,
    TournamentListComponent,
    TournamentDetailsComponent,
    NewTournamentComponent,
    TournamentRegistrationComponent,
    TournamentMatchComponent,
    TournamentNextMatchCardComponent,
    TournamentCardComponent,
    UpdateTournamentComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgTournamentTreeModule,
    NbCardModule,
    NbTabsetModule,
    NbListModule,
    NbSpinnerModule,
    NbButtonModule,
    NbRadioModule,
    NbInputModule,
    NbIconModule,
    NbTooltipModule,
    LayoutModule,
    NbUserModule,
    RouterModule,
    UserModule,
    FontAwesomeModule,
    NbPopoverModule,
    TournamentTeamsModule
  ]
})
export class TournamentsModule {
}
