import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NbToastrService} from '@nebular/theme';
import {Subject} from 'rxjs';
import {switchMap, takeUntil} from 'rxjs/operators';
import {CurrentUserApiService} from '../../user/api/current-user-api.service';
import {RoomsApiService} from '../api/rooms-api.service';
import {Room} from '../../../classes/room/room';
import {User} from '../../../classes/user/user';
import {PagedResult} from '../../../classes/paged-result';
import {LanEvent} from '../../../classes/lan-event/lan-event';
import {Filter} from '../../../filter/filter';
import {RoomFilter} from '../api/room-filter';
import {RelationalOperator} from '../../../filter/relational-operator';
import {EventsApiService} from '../../event/api/events-api.service';

@Component({
  selector: 'lan-room-main',
  templateUrl: './rooms-main.component.html',
  styleUrls: ['./rooms-main.component.scss']
})
export class RoomsMainComponent implements OnInit, OnDestroy {
  public rooms: PagedResult<Room>;
  public currentUser: User;

  constructor(private userApi: CurrentUserApiService,
              private route: ActivatedRoute,
              private router: Router,
              private roomsApi: RoomsApiService,
              private eventsApi: EventsApiService,
              private toast: NbToastrService) {
  }

  private unsubscribe: Subject<any> = new Subject<any>();

  ngOnInit() {
    this.userApi.currentUser$.pipe(
      takeUntil(this.unsubscribe.asObservable())
    ).subscribe(u => this.currentUser = u);
    // resolve route data

    this.loadRooms();
  }

  loadRooms() {
    this.eventsApi.currentEvent$.pipe(
      takeUntil(this.unsubscribe.asObservable()),
      switchMap((lanEvent: LanEvent) => {
        const filter = new Filter(RoomFilter);
        filter.setFilter('eventId', RelationalOperator.EQUALS, lanEvent.id);
        return this.roomsApi.getFilteredRooms(filter);
      })
    ).subscribe(pr => this.rooms = pr);
  }

  public async addNewRoom() {
    await this.router.navigate(['new'], {relativeTo: this.route});
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
