import {Location} from '@angular/common';
import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Router} from '@angular/router';
import {CurrentUserApiService} from '../../../user/api/current-user-api.service';
import {Room} from '../../../../classes/room/room';

@Component({
  selector: 'lan-room-list',
  templateUrl: './rooms-list.component.html',
  styleUrls: ['./rooms-list.component.scss']
})
export class RoomsListComponent {

  @Input() rooms: Room[];
  @Output() refreshRoom: EventEmitter<any> = new EventEmitter<any>();

  constructor(private router: Router,
              private currentUserApi: CurrentUserApiService,
              private location: Location) {
  }

  public currentUser$ = this.currentUserApi.currentUser$;

}
