import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, TemplateRef} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {NbDialogRef, NbDialogService} from '@nebular/theme';
import {Subject} from 'rxjs';
import {filter, take, takeUntil, tap} from 'rxjs/operators';
import {GridCoordinate} from '../../../../classes/room/grid-coordinate';
import {GridSquare} from '../../../../classes/room/grid-square';
import {PlaceUser} from '../../../../classes/room/place-user';
import {Room} from '../../../../classes/room/room';
import {User} from '../../../../classes/user/user';
import {GridSquareType} from '../../../../enums/grid-square-type';
import {UserRole} from '../../../../enums/user-role';
import {CurrentUserApiService} from '../../../user/api/current-user-api.service';
import {RoomsApiService} from '../../api/rooms-api.service';

@Component({
  selector: 'lan-room-details',
  templateUrl: './rooms-details.component.html',
  styleUrls: ['./rooms-details.component.scss']
})
export class RoomsDetailsComponent implements OnInit, OnChanges, OnDestroy {
  @Input() room: Room;
  @Output() refreshRoom: EventEmitter<any> = new EventEmitter<any>();

  constructor(private dialogService: NbDialogService,
              private currentUserApi: CurrentUserApiService,
              private router: Router,
              private roomsApi: RoomsApiService) {
  }

  public selectedGrid: GridSquare;

  public userIsManager$ = this.currentUserApi.currentUserHasRole$(UserRole.MANAGER);

  public isSeatingMode = false;

  public selectedUser: User;

  public selectedUserModal: User;
  public modalRef: NbDialogRef<any>;

  private unsubscribe = new Subject();

  ngOnInit() {
    this.router.events.pipe(
      takeUntil(this.unsubscribe),
      filter(_ => !!this.modalRef),
      filter(event => !(event instanceof NavigationEnd)),
      tap(_ => this.modalRef.close())
    ).subscribe();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('room')) {
      this.room = changes.room.currentValue;
    }
  }

  public onGridClicked(openSeatDialog: TemplateRef<any>, gridSquare: GridSquare) {
    if (gridSquare === null) {
      return;
    }
    if ([GridSquareType.SEAT_DEFAULT,
      GridSquareType.SEAT_TECH_SUPPORT,
      GridSquareType.SEAT_TOURNAMENT_SUPPORT,
      GridSquareType.SEAT_CASHIER].includes(gridSquare.type)) {
      this.selectedGrid = gridSquare;
      if (gridSquare.user || this.isSeatingMode) {
        this.modalRef = this.dialogService.open(openSeatDialog, {context: gridSquare});
        this.modalRef.onClose.pipe(take(1)).subscribe(() => this.selectedUserModal = undefined);
      }
    }
  }

  seatPlayer(user: User, gridSquare: GridSquare) {
    console.log(user);
    const placeUserMessage = new PlaceUser();
    placeUserMessage.userId = user.id;
    placeUserMessage.grid = new GridCoordinate();
    placeUserMessage.grid.x = gridSquare.x;
    placeUserMessage.grid.y = gridSquare.y;
    this.roomsApi.placeUser(gridSquare.roomId, placeUserMessage).pipe(
      filter(newGridSquare => !!newGridSquare.user),
      tap(_ => this.refreshRoom.next()),
      tap(_ => this.modalRef.close())
    ).subscribe();
    console.log(user, gridSquare);
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
  }
}
