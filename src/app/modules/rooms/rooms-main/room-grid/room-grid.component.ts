import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange} from '@angular/core';
import {faArrowUp} from '@fortawesome/free-solid-svg-icons/faArrowUp';
import {faBed} from '@fortawesome/free-solid-svg-icons/faBed';
import {faEuroSign} from '@fortawesome/free-solid-svg-icons/faEuroSign';
import {faGlassCheers} from '@fortawesome/free-solid-svg-icons/faGlassCheers';
import {faTrophy} from '@fortawesome/free-solid-svg-icons/faTrophy';
import {faTv} from '@fortawesome/free-solid-svg-icons/faTv';
import {faUser} from '@fortawesome/free-solid-svg-icons/faUser';
import {faWrench} from '@fortawesome/free-solid-svg-icons/faWrench';
import {shareReplay} from 'rxjs/operators';
import {GridSquare} from '../../../../classes/room/grid-square';
import {Room} from '../../../../classes/room/room';
import {User} from '../../../../classes/user/user';
import {GridSquareType} from '../../../../enums/grid-square-type';
import {CurrentUserApiService} from '../../../user/api/current-user-api.service';

@Component({
  selector: 'lan-room-grid',
  templateUrl: './room-grid.component.html',
  styleUrls: ['./room-grid.component.scss']
})
export class RoomGridComponent implements OnInit, OnChanges {

  @Input() room: Room;
  @Input() userHighlight: User;
  @Output() gridClicked = new EventEmitter<GridSquare>();


  constructor(private currentUserApi: CurrentUserApiService) {
  }

  public grid: Array<Array<GridSquare | null>> = [];
  public GridSquareType = GridSquareType;

  public faGlassCheers = faGlassCheers;
  public faBeamerScreen = faTv;
  public faArrowUp = faArrowUp;
  public faUser = faUser;
  public faTechSupport = faWrench;
  public faTournamentSupport = faTrophy;
  public faCashier = faEuroSign;
  public faBed = faBed;

  public currentUser$ = this.currentUserApi.currentUser$.pipe(shareReplay());

  ngOnInit() {
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    const changedRoom = changes.hasOwnProperty('room') ? changes.room : undefined;
    if (changedRoom && changedRoom.isFirstChange()) {
      const roomInput: Room = changedRoom.currentValue;
      for (let i = 0; i < roomInput.sizeY; i++) {
        this.grid[i] = [];
        for (let j = 0; j < roomInput.sizeX; j++) {
          this.grid[i][j] = null;
        }
      }
      roomInput.gridSquares.forEach(value => {
        this.grid[value.y - 1][value.x - 1] = value;
      });
    }
  }

  public getXCoordinateList(): number[] {
    return this.room && this.room.sizeX ? Array(this.room.sizeX) : [];
  }

  public clickGrid(square: GridSquare) {
    this.gridClicked.next(square);
  }

  public asUser(user: User): User {
    return user;
  }
}
