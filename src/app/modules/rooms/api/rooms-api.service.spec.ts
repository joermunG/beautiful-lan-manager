import { TestBed } from '@angular/core/testing';

import { RoomsApiService } from './rooms-api.service';

describe('RoomsApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RoomsApiService = TestBed.get(RoomsApiService);
    expect(service).toBeTruthy();
  });
});
