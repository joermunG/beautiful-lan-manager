import {AbstractFilter} from '../../../filter/abstract-filter';
import {RelationalOperator} from '../../../filter/relational-operator';

export class RoomFilter extends AbstractFilter {
  public id: Map<RelationalOperator, number | number[]> = new Map<RelationalOperator, number | number[]>();
  public name: Map<RelationalOperator, string> = new Map<RelationalOperator, string>();
  public eventId: Map<RelationalOperator, number | number[]> = new Map<RelationalOperator, number | number[]>();
}
