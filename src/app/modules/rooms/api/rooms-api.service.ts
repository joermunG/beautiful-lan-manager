import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {NbToastrService} from '@nebular/theme';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {AbstractApiService} from '../../../abstract-services/abstract-api.service';
import {PagedResult} from '../../../classes/paged-result';
import {CreateRoom} from '../../../classes/room/create-room';
import {GridSquare} from '../../../classes/room/grid-square';
import {PlaceUser} from '../../../classes/room/place-user';
import {Room} from '../../../classes/room/room';
import {UpdateGrid} from '../../../classes/room/update-grid';
import {UpdateRoom} from '../../../classes/room/update-room';
import {Filter} from '../../../filter/filter';
import {RelationalOperator} from '../../../filter/relational-operator';
import {RoomFilter} from './room-filter';

@Injectable({
  providedIn: 'root'
})
export class RoomsApiService extends AbstractApiService<Room> {

  baseUrl = '/api/v1/Rooms';

  constructor(public http: HttpClient, private toast: NbToastrService) {
    super(Room);
  }

  public getRoomById(id: number): Observable<Room> {
    const filter = new Filter(RoomFilter, 1, 1);
    filter.setFilter('id', RelationalOperator.EQUALS, id);
    return this.getFiltered(this.baseUrl, filter).pipe(
      map(r => {
        if (r.rowCount !== 1) {
          throwError(new Error('Room with id ' + id + ' not found'));
        } else {
          return r;
        }
      }),
      map(pagedResult => pagedResult.results.shift())
    );
  }

  public getFilteredRooms(filter: Filter<RoomFilter>): Observable<PagedResult<Room>> {
    return this.getFiltered(this.baseUrl, filter);
  }

  public createRoom(createMessage: CreateRoom): Observable<Room> {
    return this.postGeneric(this.baseUrl, this.jsonConvert.deserializeObject(createMessage, CreateRoom), Room).pipe(
      catchError((err) => {
        this.toast.danger(createMessage.name, 'Creation Failed!');
        return throwError(err);
      })
    );
  }

  public updateRoom(roomId: number, updateMessage: UpdateRoom): Observable<Room> {
    return this.putGeneric(this.baseUrl + '/' + roomId, this.jsonConvert.deserializeObject(updateMessage, UpdateRoom), Room).pipe(
      catchError((err) => {
        this.toast.danger(roomId, 'Update Failed!');
        return throwError(err);
      })
    );
  }

  public deleteRoom(roomId: number): Observable<null> {
    return super.deleteGeneric<null>(this.baseUrl + '/' + roomId, null).pipe(
      catchError((err) => {
        this.toast.danger(roomId, 'Deletion of Tournament Failed!');
        return throwError(err);
      })
    );
  }

  public updateGrid(roomId: number, updateGridMessage: UpdateGrid): Observable<GridSquare> {
    return this.putGeneric(
      this.baseUrl + '/' + roomId + '/update-grid',
      this.jsonConvert.deserializeObject(updateGridMessage, UpdateGrid),
      GridSquare
    ).pipe(
      catchError((err) => {
        this.toast.danger(roomId, 'Update Failed!');
        return throwError(err);
      })
    );
  }

  public placeUser(roomId: number, placeUserMessage: PlaceUser): Observable<GridSquare> {
    return this.putGeneric(
      this.baseUrl + '/' + roomId + '/place-user',
      this.jsonConvert.deserializeObject(placeUserMessage, PlaceUser),
      GridSquare
    ).pipe(
      catchError((err) => {
        this.toast.danger(roomId, 'User could not be seated!');
        return throwError(err);
      })
    );
  }
}
