import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {
  NbButtonModule,
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbListModule,
  NbRadioModule, NbSelectModule,
  NbSpinnerModule,
  NbTabsetModule,
  NbTooltipModule,
  NbUserModule
} from '@nebular/theme';
import {NgSelectModule} from '@ng-select/ng-select';
import {NgTournamentTreeModule} from 'ng-tournament-tree';
import {LayoutModule} from '../layout-module/layout.module';
import {UserModule} from '../user/user.module';
import {RoomsMainComponent} from './rooms-main/rooms-main.component';
import {RoomsListComponent} from './rooms-main/rooms-list/rooms-list.component';
import {RoomsDetailsComponent} from './rooms-main/rooms-details/rooms-details.component';
import {NewRoomComponent} from './new-room/new-room.component';
import {RouterModule} from '@angular/router';
import {RoomGridComponent} from './rooms-main/room-grid/room-grid.component';

@NgModule({
  declarations: [
    RoomsMainComponent,
    RoomsListComponent,
    RoomsDetailsComponent,
    NewRoomComponent,
    RoomGridComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgTournamentTreeModule,
    NbCardModule,
    NbTabsetModule,
    NbListModule,
    NbSpinnerModule,
    NbButtonModule,
    NbRadioModule,
    NbInputModule,
    NbIconModule,
    NbTooltipModule,
    LayoutModule,
    NbUserModule,
    RouterModule,
    NbSelectModule,
    FontAwesomeModule,
    NgSelectModule,
    UserModule
  ]
})
export class RoomsModule {
}
