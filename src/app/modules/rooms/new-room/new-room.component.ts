import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {NbComponentStatus, NbToastrService} from '@nebular/theme';
import {Subject} from 'rxjs';
import {take, tap} from 'rxjs/operators';
import {EventsApiService} from '../../event/api/events-api.service';
import {RoomsApiService} from '../api/rooms-api.service';
import {CreateRoom} from '../../../classes/room/create-room';

@Component({
  selector: 'lan-new-room',
  templateUrl: './new-room.component.html',
  styleUrls: ['./new-room.component.scss']
})
export class NewRoomComponent implements OnInit, OnDestroy {

  constructor(private roomsApi: RoomsApiService,
              private eventsApi: EventsApiService,
              private toast: NbToastrService,
              private router: Router) {
  }

  public form = new FormGroup({
    name: new FormControl(null,
      {
        validators: Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(100)])
      }
    ),
    sizeX: new FormControl(null,
      {
        validators: Validators.compose([Validators.required, Validators.min(1)])
      }
    ),
    sizeY: new FormControl(null,
      {
        validators: Validators.compose([Validators.required, Validators.min(1)])
      }
    ),
    eventId: new FormControl(null)
  });

  private currentEvent$ = this.eventsApi.currentEvent$;
  private unsubscribe: Subject<any> = new Subject<any>();

  ngOnInit() {
    this.currentEvent$.pipe(
      take(1),
      tap(event => {
        const formData = new CreateRoom();
        formData.eventId = event.id;
        this.form.patchValue({
          name: formData.name,
          sizeX: formData.sizeX,
          sizeY: formData.sizeY,
          eventId: formData.eventId,
        });
      })
    ).subscribe();
  }

  public submit() {
    if (this.form.valid) {
      this.roomsApi.createRoom(this.form.getRawValue()).pipe(
        take(1),
        tap(resp => {
          this.toast.success(resp.name, 'New Room Created', {
            destroyByClick: true,
            duration: 10000,
            hasIcon: true,
            icon: 'checkmark-circle-outline'
          });
          this.router.navigate(['/rooms']);
        })
      ).subscribe();
    }
  }

  public formFieldStatus(fieldName: string): NbComponentStatus {
    const field = this.form.get(fieldName);
    if (!field.touched) {
      return null;
    }
    if (field.touched && field.invalid) {
      return 'danger';
    }
    if (field.touched && field.valid) {
      return 'primary';
    }
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
