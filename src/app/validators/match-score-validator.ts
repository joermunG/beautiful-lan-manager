import {FormArray} from '@angular/forms';

export function MatchScoreValidator(control: FormArray) {
  const errorMsg = {noWinner: true};
  let isValid = false;
  const sortedValues = [...control.value].sort((a, b) => {
    return a < b ? 1 : a === b ? 0 : -1;
  });

  if (sortedValues[0] > sortedValues[1]) {
    isValid = true;
  }
  return isValid ? null : errorMsg;
}
