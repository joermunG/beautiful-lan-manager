import {HttpClient} from '@angular/common/http';
import {JsonConvert, OperationMode, ValueCheckingMode} from 'json2typescript';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {PagedResult} from '../classes/paged-result';
import {AbstractFilter} from '../filter/abstract-filter';
import {Filter} from '../filter/filter';

export abstract class AbstractApiService<T> {

  public allEntries = new BehaviorSubject<T[]>([]);
  abstract baseUrl: string;
  abstract http: HttpClient;
  protected jsonConvert: JsonConvert;
  private readonly classT;

  protected constructor(classT: new() => T) {
    this.jsonConvert = new JsonConvert();
    this.jsonConvert.operationMode = OperationMode.ENABLE; // print some debug data
    this.jsonConvert.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
    this.jsonConvert.valueCheckingMode = ValueCheckingMode.ALLOW_NULL; // always allow null
    this.classT = (new classT()).constructor;
  }

  protected getFiltered<X extends AbstractFilter>(url: string, filter: Filter<X>): Observable<PagedResult<T>> {
    const finalUrl = url + filter.getQueryString(true);
    const response = this.http.get<T[]>(finalUrl, {observe: 'response'});
    return response.pipe(
      map(r => {
        const pagedResult = new PagedResult<T>();
        pagedResult.currentPage = Number(r.headers.get('x-current-page'));
        pagedResult.pageCount = Number(r.headers.get('x-page-count'));
        pagedResult.pageSize = Number(r.headers.get('x-page-size'));
        pagedResult.rowCount = Number(r.headers.get('x-total-count'));
        pagedResult.results = this.jsonConvert.deserializeArray(r.body, this.classT);
        return pagedResult;
      })
    );
  }

  protected postGeneric<X, Y>(url: string, createMessage: X, respClass: new() => Y): Observable<Y> {
    const msg = this.jsonConvert.serializeObject(createMessage);
    return this.http.post(url, msg).pipe(
      map(resp => this.jsonConvert.deserializeObject(resp, respClass))
    );
  }

  protected putGeneric<X, Y>(url: string, updateMessage: X, respClass: new() => Y): Observable<Y> {
    return this.http.put<X>(url, this.jsonConvert.serializeObject(updateMessage)).pipe(
      map(resp => this.jsonConvert.deserializeObject(resp, respClass))
    );
  }

  protected deleteGeneric<Y>(url: string, respClass: new() => Y): Observable<Y> {
    return this.http.delete(url).pipe(
      map((resp) => this.jsonConvert.deserializeObject(resp, respClass))
    );
  }
}
