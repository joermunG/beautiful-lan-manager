import {Injectable} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {Observable, Subject} from 'rxjs';
import {filter, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClearCachesService {

  constructor(private router: Router) {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd && event.url === '/auth/logout'),
      tap(onLogout => this.clearCachesEvent.next())
    ).subscribe();
  }

  public clearCachesEvent: Subject<any> = new Subject<any>();
  public clearCache$: Observable<any> = this.clearCachesEvent.asObservable();
}
